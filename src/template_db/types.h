/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___TEMPLATE_DB__TYPES_H
#define DE__OSM3S___TEMPLATE_DB__TYPES_H

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#ifdef NATIVE_LARGE_FILES
#define ftruncate64 ftruncate
#define lseek64 lseek
#define open64 open
#endif

typedef unsigned int uint;

typedef char int8;
typedef short int int16;
typedef int int32;
typedef long long int64;

typedef unsigned char uint8;
typedef unsigned short int uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef uint32 timestamp_t;

const int S_666 = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
const int S_664 = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH;
const int S_644 = S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH;
const int S_444 = S_IRUSR|S_IRGRP|S_IROTH;

#ifdef __has_builtin
#  define OVERPASS_HAS_BUILTIN(x)     __has_builtin(x)
#else
#  define OVERPASS_HAS_BUILTIN(x)     0
#endif

template <typename T>
inline T unalignedLoad(const void *ptr)
{
    T result;
#if OVERPASS_HAS_BUILTIN(__builtin_memcpy)
    __builtin_memcpy
#else
    std::memcpy
#endif
    /*memcpy*/(&result, ptr, sizeof result);
    return result;
}

template <typename T>
inline void unalignedStore(void *ptr, T t)
{
#if OVERPASS_HAS_BUILTIN(__builtin_memcpy)
    __builtin_memcpy
#else
    memcpy
#endif
    /*memcpy*/(ptr, &t, sizeof t);
}

#undef OVERPASS_HAS_BUILTIN

// Generic helper functions to print contents of std::map and std::vector

template < typename... Args >
std::ostream& operator << (std::ostream& stream, const std::vector <Args...> & container)
{
  stream << "{ ";
  for (auto&& elem : container) stream << elem << " ";
  stream << "}";
  return stream;
}

template < typename... Args >
std::ostream& operator << (std::ostream& stream, const std::set <Args...> & container)
{
  stream << "{ ";
  for (auto&& elem : container) stream << elem << " ";
  stream << "}";
  return stream;
}


template < typename... Args >
std::ostream& operator << (std::ostream& stream, const std::map <Args...> & container)
{
  stream << "{ ";
  for (auto&& elem : container) stream << elem << " ";
  stream << "}";
  return stream;
}

template<typename T1, typename T2>
std::ostream& operator << (std::ostream& stream, const std::pair <T1, T2> & val)
{
  stream << "{" << val.first << " " << val.second << "}";
  return stream;
}



struct Rate_limited_Error : std::exception {


  Rate_limited_Error(const std::string& filename_, const std::string& origin_) :
    filename(filename_), origin(origin_){

    std::ostringstream out;
    out << origin <<' ' << filename;
    error_msg = out.str();

  }

  const char* what() const noexcept override {
    return error_msg.c_str();
  }

  std::string filename;
  std::string origin;
  std::string error_msg;
};

struct Timeout_Error : std::exception {


  Timeout_Error(const std::string& filename_, const std::string& origin_) :
    filename(filename_), origin(origin_) {

    std::ostringstream out;
    out << origin <<' ' << filename;
    error_msg = out.str();
  }

  const char* what() const noexcept override {
    return error_msg.c_str();
  }

  std::string filename;
  std::string origin;
  std::string error_msg;
};

struct File_Error : std::exception
{
  File_Error(uint32 errno_, const std::string& filename_, const std::string& origin_)
  : error_number(errno_), filename(filename_), origin(origin_) {

    std::ostringstream out;
    out << origin <<' ' << filename<<' '<< error_number<<' '<< strerror(error_number);
    error_msg = out.str();
  }

  const char* what() const noexcept override {
    return error_msg.c_str();
  }

  uint32 error_number;
  std::string filename;
  std::string origin;
  std::string error_msg;
};


struct File_Properties_Exception
{
  File_Properties_Exception(int32 i) : id(i) {}

  int32 id;
};

enum class Block_Compression {
    USE_DEFAULT = -1,
    NO_COMPRESSION = 0,
    ZLIB_COMPRESSION = 1,
    LZ4_COMPRESSION = 2
};

struct File_Blocks_Index_Base
{
  virtual bool empty() const = 0;
  virtual ~File_Blocks_Index_Base() = default;

  virtual bool writeable() const = 0;
  virtual const std::string& file_name_extension() const = 0;

  virtual const std::string& get_data_file_name() const = 0;
  virtual uint64 get_block_size() const = 0;
  virtual uint32 get_compression_factor() const = 0;
  virtual Block_Compression get_compression_method() const = 0;
};


struct File_Properties
{
  virtual const std::string& get_file_name_trunk() const = 0;
  virtual const std::string& get_index_suffix() const = 0;
  virtual const std::string& get_data_suffix() const = 0;
  virtual const std::string& get_id_suffix() const = 0;
  virtual const std::string& get_shadow_suffix() const = 0;
  virtual uint32 get_block_size() const = 0;
  virtual uint32 get_compression_factor() const = 0;
  virtual Block_Compression get_compression_method() const = 0;
  virtual uint32 get_map_block_size() const = 0;
  virtual uint32 get_map_compression_factor() const = 0;
  virtual Block_Compression get_map_compression_method() const = 0;
  virtual std::vector< bool > get_data_footprint(const std::string& db_dir) const = 0;
  virtual std::vector< bool > get_map_footprint(const std::string& db_dir) const = 0;

  // The returned object is of type File_Blocks_Index< .. >*
  // and goes into the ownership of the caller.
  virtual File_Blocks_Index_Base* new_data_index
      (bool writeable, bool use_shadow, const std::string& db_dir, const std::string& file_name_extension)
      const = 0;

  virtual ~File_Properties() = default;
};


/** Simple RAII class to keep a file descriptor. */
class Raw_File
{
  public:
    Raw_File(const Raw_File&) = delete;
    Raw_File operator=(const Raw_File&) = delete;

    Raw_File(const std::string& name, int oflag, mode_t mode, const char* caller_id);
    ~Raw_File() { close(fd_); }
    int fd() const { return fd_; }
    uint64 size(const char* caller_id) const;
    void resize(uint64 size, const char* caller_id) const;
    void read(void* buf, uint64 size, const char* caller_id) const;
    void write(void* buf, uint64 size, const char* caller_id) const;
    void seek(uint64 pos, const char* caller_id) const;

  private:
    int fd_;
    std::string name;
};



inline bool file_exists(const std::string& filename)
{
  return (access(filename.c_str(), F_OK) == 0);
}


/** Simple RAII class to keep a unix socket. */
class Unix_Socket
{
public:
  Unix_Socket(const std::string& socket_name = "", uint max_num_reading_processes = 0);
  void open(const std::string& socket_name);
  ~Unix_Socket();

  int descriptor() const { return socket_descriptor; }

private:
  int socket_descriptor;
  uint max_num_reading_processes;
};


//-----------------------------------------------------------------------------

inline Raw_File::Raw_File(const std::string& name_, int oflag, mode_t mode, const char* caller_id)
  : fd_(0), name(name_)
{
  fd_ = open64(name.c_str(), oflag, mode);
  if (fd_ < 0)
    throw File_Error(errno, name, caller_id);
  if (mode & O_CREAT)
  {
    int ret = fchmod(fd_, mode);
    if (ret < 0)
      throw File_Error(errno, name, std::string(caller_id) + "::fchmod");
  }
}

inline uint64 Raw_File::size(const char* caller_id) const
{
  off64_t size = lseek64(fd_, 0, SEEK_END);
  if (size < 0)
    throw File_Error(errno, name, caller_id);
  off64_t foo = lseek64(fd_, 0, SEEK_SET);
  if (foo != 0)
    throw File_Error(errno, name, caller_id);
  return size;
}

inline void Raw_File::resize(uint64 size, const char* caller_id) const
{
  uint64 foo = ftruncate64(fd_, size);
  if (foo != 0)
    throw File_Error(errno, name, caller_id);
}

inline void Raw_File::read(void* buf, uint64 size, const char* caller_id) const
{
  uint64 foo = ::read(fd_, buf, size);
  if (foo != size)
    throw File_Error(errno, name, caller_id);
}

inline void Raw_File::write(void* buf, uint64 size, const char* caller_id) const
{
  uint64 foo = ::write(fd_, buf, size);
  if (foo != size)
    throw File_Error(errno, name, caller_id);
}

inline void Raw_File::seek(uint64 pos, const char* caller_id) const
{
  uint64 foo = lseek64(fd_, pos, SEEK_SET);
  if (foo != pos)
    throw File_Error(errno, name, caller_id);
}

//-----------------------------------------------------------------------------


template< typename Int >
int shift_log(Int val)
{
  int count = 0;
  while (val > 1)
  {
    val = val>>1;
    ++count;
  }
  return count;
}


template< typename Iterator, typename Object >
void rearrange_block(const Iterator& begin, Iterator& it, Object to_move)
{
  Iterator predecessor = it;
  if (it != begin)
    --predecessor;
  while (to_move < *predecessor)
  {
    *it = *predecessor;
    --it;
    if (it == begin)
      break;
    --predecessor;
  }
  *it = to_move;
}


inline void zero_padding(uint8* from, uint32 bytes)
{
  for (uint32 i = 0; i < bytes; ++i)
    *(from + i) = 0;
}


//-----------------------------------------------------------------------------


int& global_read_counter();

bool& fastcgi_enabled();


void millisleep(uint32 milliseconds);


void copy_file(const std::string& source, const std::string& dest);
void rename_file(const std::string& source, const std::string& dest);


#endif
