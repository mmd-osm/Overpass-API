/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___TEMPLATE_DB__BLOCK_BACKEND_UPDATER_H
#define DE__OSM3S___TEMPLATE_DB__BLOCK_BACKEND_UPDATER_H

#include "block_backend.h"
#include "file_blocks.h"
#include "types.h"

#include <algorithm>
#include <cstring>
#include <map>
#include <memory>
#include <set>
#include <type_traits>



template< class TIndex, class TObject, template<class...> class TContainer >
struct Index_Collection
{
  Index_Collection(uint8* source_begin_, uint8* source_end_,
                   const typename std::map< TIndex, TContainer< TObject > >::const_iterator& delete_it_,
                   const typename std::map< TIndex, TContainer< TObject > >::const_iterator& insert_it_)
      : source_begin(source_begin_), source_end(source_end_),
        delete_it(delete_it_), insert_it(insert_it_) {}

  uint8* source_begin;
  uint8* source_end;
  typename std::map< TIndex, TContainer< TObject > >::const_iterator delete_it;
  typename std::map< TIndex, TContainer< TObject > >::const_iterator insert_it;
};


template< class TIndex, class TObject, class TIterator = typename std::set< TIndex >::const_iterator >
struct Block_Backend_Updater
{
    typedef File_Blocks< TIndex, TIterator > File_Blocks_;

    Block_Backend_Updater(File_Blocks_Index_Base* index_);
    ~Block_Backend_Updater();

    template< template<class...> class TContainer >
    void update
        (const std::map< TIndex, TContainer< TObject > >& to_delete,
         const std::map< TIndex, TContainer< TObject > >& to_insert);

  private:
    File_Blocks_ file_blocks;
    uint32 block_size;

    void calc_split_idxs
        (std::vector< TIndex >& split,
         const std::vector< uint32 >& sizes,
         typename std::set< TIndex >::const_iterator it,
         const typename std::set< TIndex >::const_iterator& end);

    void flush_if_necessary_and_write_obj(
        uint64* start_ptr, uint8*& insert_ptr, typename File_Blocks_::Write_Iterator& file_it,
        const TIndex& idx, const TObject& obj);

    template< template<class...> class TContainer >
    void create_from_scratch
        (typename File_Blocks_::Write_Iterator& file_it,
         const std::map< TIndex, TContainer< TObject > >& to_insert);

    template< template<class...> class TContainer >
    void update_group
        (typename File_Blocks_::Write_Iterator& file_it,
         const std::map< TIndex, TContainer< TObject > >& to_delete,
         const std::map< TIndex, TContainer< TObject > >& to_insert);

    template< template<class...> class TContainer >
    uint32 skip_deleted_objects(
        uint64* source_start_ptr, uint64* dest_start_ptr,
        const TContainer < TObject >& objs_to_delete, uint32 idx_size, const TIndex& idx);

    bool read_block_or_blocks(
        typename File_Blocks_::Write_Iterator& file_it, std::unique_ptr<uint64[]>& source, uint32& buffer_size);

    void flush_or_delete_block(
        uint64* start_ptr, uint bytes_written, typename File_Blocks_::Write_Iterator& file_it,
        uint32 idx_size);

    template< template<class...> class TContainer >
    void update_segments
        (typename File_Blocks_::Write_Iterator& file_it,
         const std::map< TIndex, TContainer< TObject > >& to_delete,
         const std::map< TIndex, TContainer< TObject > >& to_insert);

};


template< class TIndex, class TObject, class TIterator >
Block_Backend_Updater< TIndex, TObject, TIterator >::Block_Backend_Updater(File_Blocks_Index_Base* index_)
  : file_blocks(index_),
    block_size(index_->get_block_size() * index_->get_compression_factor()) {}


template< class TIndex, class TObject, class TIterator >
Block_Backend_Updater< TIndex, TObject, TIterator >::~Block_Backend_Updater()
= default;


template< class TIndex, class TObject, class TIterator >
template< template<class...> class TContainer  >
void Block_Backend_Updater< TIndex, TObject, TIterator >::update
    (const std::map< TIndex, TContainer< TObject > >& to_delete,
     const std::map< TIndex, TContainer< TObject > >& to_insert)
{
  std::set< TIndex > relevant_idxs;

  auto hint_it = relevant_idxs.begin();
  for (auto it(to_insert.begin()); it != to_insert.end(); ++it)
    hint_it = relevant_idxs.emplace_hint(hint_it, it->first);

  for (auto it(to_delete.begin()); it != to_delete.end(); ++it)
    relevant_idxs.insert(it->first);

  typename File_Blocks_::Write_Iterator file_it
      = file_blocks.write_begin(relevant_idxs.begin(), relevant_idxs.end(), true);

  while (file_it.lower_bound() != file_it.idx_end())
  {
    if (file_it.block_type() == Index_Block_Type::EMPTY)
      create_from_scratch(file_it, to_insert);
    else if (file_it.block_type() == Index_Block_Type::GROUP)
      update_group(file_it, to_delete, to_insert);
    else //if (file_it.block_type() == File_Block_Index_Entry< TIndex >::SEGMENT)
      update_segments(file_it, to_delete, to_insert);
  }
}

template< class TIndex, class TObject, class TIterator >
void Block_Backend_Updater< TIndex, TObject, TIterator >::calc_split_idxs
    (std::vector< TIndex >& split,
     const std::vector< uint32 >& sizes,
     typename std::set< TIndex >::const_iterator it,
     const typename std::set< TIndex >::const_iterator& end)
{
  std::vector< uint32 > vsplit;
  std::vector< uint64 > min_split_pos;

  // calc total size
  uint64 total_size(0);
  for (uint i(0); i < sizes.size(); ++i)
    total_size += sizes[i];

  // calc minimal splitting points
  uint64 cur_size(0), sum_size(0);
  if (!sizes.empty())
    cur_size = sizes[sizes.size() - 1];
  for (int i(sizes.size()-2); i >= 0; --i)
  {
    cur_size += sizes[i];
    if (cur_size <= block_size-4)
      continue;
    sum_size += cur_size - sizes[i];
    min_split_pos.push_back(total_size - sum_size);
    cur_size = sizes[i];
  }

  std::vector< uint64 > oversize_splits;
  // find oversized blocks and force splits there
  sum_size = 0;
  if (!sizes.empty())
    sum_size = sizes[0];
  bool split_after((!sizes.empty()) && (sizes[0] > block_size - 4));
  for (uint i(1); i < sizes.size(); ++i)
  {
    if (sizes[i] > block_size - 4)
    {
      oversize_splits.push_back(sum_size);
      split_after = true;
    }
    else if (split_after)
    {
      oversize_splits.push_back(sum_size);
      split_after = false;
    }
    sum_size += sizes[i];
  }
  oversize_splits.push_back(sum_size);

  std::vector< std::pair< uint64, uint32 > > forced_splits;
  // find splitting points where the average is below the minimum
  // - here needs the fitting to be corrected
  sum_size = 0;
  int min_split_i(min_split_pos.size());
  for (std::vector< uint64 >::const_iterator oit(oversize_splits.begin());
  oit != oversize_splits.end(); ++oit)
  {
    int block_count(1);
    while ((min_split_i - block_count >= 0) &&
      (min_split_pos[min_split_i - block_count] < *oit))
      ++block_count;
    // correct the fitting if necessary
    uint32 used_blocks(0);
    for (int j(1); j < block_count; ++j)
    {
      if ((*oit - sum_size)*j/block_count + sum_size
        <= min_split_pos[min_split_i - j])
      {
        forced_splits.push_back
          (std::make_pair(min_split_pos[min_split_i - j], j - used_blocks));
        used_blocks = j;
      }
    }
    forced_splits.push_back(std::make_pair(*oit, block_count - used_blocks));
    min_split_i = min_split_i - block_count;
    sum_size = *oit;
  }

  std::vector< std::pair< uint64, uint32 > >::const_iterator forced_it(forced_splits.begin());
  // calculate the real splitting positions
  sum_size = 0;
  uint64 min_sum_size(0);
  uint32 cur_block(0);
  uint64 next_limit(forced_it->first/forced_it->second);
  for (uint i(0); i < sizes.size(); ++i)
  {
    sum_size += sizes[i];
    if (sum_size > forced_it->first)
    {
      vsplit.push_back(i);
      cur_block = 0;
      min_sum_size = forced_it->first;
      ++forced_it;
      next_limit = (forced_it->first - min_sum_size)/
      (forced_it->second - cur_block) + min_sum_size;
      uint j(min_split_pos.size() - 1 - vsplit.size());
      if ((vsplit.size() < min_split_pos.size()) &&
        (min_split_pos[j] > next_limit))
        next_limit = min_split_pos[j];
    }
    else if (sum_size > next_limit)
    {
      vsplit.push_back(i);
      ++cur_block;
      min_sum_size = sum_size - sizes[i];
      next_limit = (forced_it->first - min_sum_size)/
      (forced_it->second - cur_block) + min_sum_size;
      uint j(min_split_pos.size() - 1 - vsplit.size());
      if ((vsplit.size() < min_split_pos.size()) &&
        (min_split_pos[j] > next_limit))
        next_limit = min_split_pos[j];
    }
  }

  // This converts the result in a more convienient form
  uint i(0), j(0);
  while ((j < vsplit.size()) && (it != end))
  {
    if (vsplit[j] == i)
    {
      split.push_back(*it);
      ++j;
    }

    ++i;
    ++it;
  }
}


template< class Index, class Object, class TIterator >
void Block_Backend_Updater< Index, Object, TIterator >::flush_if_necessary_and_write_obj(
    uint64* start_ptr, uint8*& insert_ptr, typename File_Blocks_::Write_Iterator& file_it,
    const Index& idx, const Object& obj)
{
  uint32 idx_size = idx.size_of();
  uint32 obj_size = obj.size_of();

  if (insert_ptr - (uint8*)start_ptr + obj_size > block_size)
  {
    uint bytes_written = insert_ptr - (uint8*)start_ptr;
    if (bytes_written > 8 + idx_size)
    {
      *(uint32*)start_ptr = bytes_written;
      *(((uint32*)start_ptr)+1) = bytes_written;
      file_it = file_blocks.insert_block(file_it, start_ptr, bytes_written - 4);
    }
    if (idx_size + obj_size + 8 > block_size)
    {
      if (obj_size > 64*1024*1024)
          throw File_Error(0, file_blocks.get_index().get_data_file_name(), "Block_Backend: an item's size exceeds limit of 64 MiB.");

      uint buf_scale = (idx_size + obj_size + 7)/block_size + 1;
      std::unique_ptr<uint64[]> large_buf(new uint64[buf_scale * block_size / 8]);
      *(uint32*)large_buf.get() = block_size;
      *(((uint32*)large_buf.get())+1) = idx_size + obj_size + 8;
      memcpy(large_buf.get()+1, start_ptr+1, idx_size);
      obj.to_data(((uint8*)large_buf.get()) + 8 + idx_size);

      for (uint i = 0; i+1 < buf_scale; ++i)
      {
        file_it = file_blocks.insert_block(
            file_it, large_buf.get() + i*block_size/8, block_size,
            i == 0 ? idx_size + obj_size + 4 : 0, idx);
      }
      file_it = file_blocks.insert_block(
          file_it, large_buf.get() + (buf_scale-1)*block_size/8, idx_size + obj_size + 8 - block_size*(buf_scale-1),
          0, idx);

      insert_ptr = ((uint8*)start_ptr) + 8 + idx_size;
      return;
    }
    else
      insert_ptr = ((uint8*)start_ptr) + 8 + idx_size;
  }

  obj.to_data(insert_ptr);
  insert_ptr = insert_ptr + obj_size;
}


template< class TIndex, class TObject, class TIterator >
template<template<class...> class TContainer >
void Block_Backend_Updater< TIndex, TObject, TIterator >::create_from_scratch
    (typename File_Blocks_::Write_Iterator& file_it,
     const std::map< TIndex, TContainer< TObject > >& to_insert)
{
  std::map< TIndex, uint32 > sizes;
  std::vector< TIndex > split;
  std::vector< uint32 > vsizes;
  auto buffer = std::unique_ptr<uint8[]>(new uint8[block_size]);

  // compute the distribution over different blocks
  for (typename std::set< TIndex >::const_iterator fit(file_it.lower_bound());
      fit != file_it.upper_bound(); ++fit)
  {
    auto it(to_insert.find(*fit));

    uint32 current_size(4);
    if ((it == to_insert.end()) || (it->second.empty()))
      current_size = 0;
    else
    {
      // only add nonempty indices
      current_size += fit->size_of();
      for (auto it2(it->second.begin());
          it2 != it->second.end(); ++it2)
        current_size += it2->size_of();
    }

    sizes[*fit] += current_size;
    vsizes.push_back(current_size);
  }
  calc_split_idxs(split, vsizes, file_it.lower_bound(), file_it.upper_bound());

  // really write data
  typename std::vector< TIndex >::const_iterator split_it(split.begin());
  uint8* pos(buffer.get() + 4);
  uint32 max_size(0);
  typename std::set< TIndex >::const_iterator upper_bound(file_it.upper_bound());
  for (typename std::set< TIndex >::const_iterator fit(file_it.lower_bound());
      fit != upper_bound; ++fit)
  {
    auto it(to_insert.find(*fit));

    if ((split_it != split.end()) && (*fit == *split_it))
    {
      if (pos > buffer.get() + 4)
      {
        *(uint32*)buffer.get() = pos - buffer.get();
        file_it = file_blocks.insert_block(file_it, (uint64*)buffer.get(), max_size);
        pos = buffer.get() + 4;
      }
      ++split_it;
      max_size = 0;
    }

    if (sizes[*fit] > max_size)
      max_size = sizes[*fit];

    if (sizes[*fit] == 0)
      continue;
    else if (sizes[*fit] < block_size - 4)
    {
      uint8* current_pos(pos);
      fit->to_data(pos + 4);
      pos = pos + fit->size_of() + 4;
      if (it != to_insert.end())
      {
        for (auto it2(it->second.begin()); it2 != it->second.end(); ++it2)
        {
          it2->to_data(pos);
          pos = pos + it2->size_of();
        }
      }
      unalignedStore(current_pos, uint32(pos - buffer.get()));
    }
    else
    {
      fit->to_data(pos + 4);
      pos = pos + fit->size_of() + 4;

      if (it != to_insert.end())
      {
        for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
          flush_if_necessary_and_write_obj(
              (uint64*)buffer.get(), pos, file_it, *fit, *it2);
      }

      if (pos - buffer.get() > fit->size_of() + 8)
      {
        *(uint32*)(buffer.get() + 4) = pos - buffer.get();
        max_size = (unalignedLoad<uint32>(buffer.get() + 4)) - 4;
      }
      else
        pos = buffer.get() + 4;
    }
  }
  if (pos > buffer.get() + 4)
  {
    *(uint32*)buffer.get() = pos - buffer.get();
    file_it = file_blocks.insert_block(file_it, (uint64*)buffer.get(), max_size);
  }
  ++file_it;
}


template< class TIndex, class TObject, class TIterator >
template< template<class...> class TContainer >
void Block_Backend_Updater< TIndex, TObject, TIterator  >::update_group
    (typename File_Blocks_::Write_Iterator& file_it,
     const std::map< TIndex, TContainer< TObject > >& to_delete,
     const std::map< TIndex, TContainer< TObject > >& to_insert)
{
  std::map< TIndex, Index_Collection< TIndex, TObject, TContainer > > index_values;
  std::map< TIndex, uint32 > sizes;
  std::vector< TIndex > split;
  std::vector< uint32 > vsizes;

  auto source = std::unique_ptr<uint8[]>(new uint8[block_size]);
  auto dest = std::unique_ptr<uint8[]>(new uint8[block_size]);

  file_blocks.read_block(file_it, (uint64*)source.get());

  // prepare a unified iterator over all indices, from file, to_delete
  // and to_insert
  uint8* pos(source.get() + 4);
  uint8* source_end(source.get() + unalignedLoad<uint32>(source.get()));
  while (pos < source_end)
  {
    index_values.insert(std::make_pair(TIndex(pos + 4), Index_Collection< TIndex, TObject, TContainer >
        (pos, source.get() + unalignedLoad<uint32>(pos), to_delete.end(), to_insert.end())));
    pos = source.get() + unalignedLoad<uint32>(pos);
  }
  auto to_delete_begin(to_delete.lower_bound(*(file_it.lower_bound())));
  auto to_delete_end(to_delete.end());
  if (file_it.upper_bound() != file_it.idx_end())
    to_delete_end = to_delete.lower_bound(*(file_it.upper_bound()));
  for (auto it(to_delete_begin); it != to_delete_end; ++it)
  {
    auto ic_it(index_values.find(it->first));
    if (ic_it == index_values.end())
    {
      index_values.insert(std::make_pair(it->first,
          Index_Collection< TIndex, TObject, TContainer >(nullptr, nullptr, it, to_insert.end())));
    }
    else
      ic_it->second.delete_it = it;
  }

  auto to_insert_begin(to_insert.lower_bound(*(file_it.lower_bound())));
  auto to_insert_end(to_insert.end());
  if (file_it.upper_bound() != file_it.idx_end())
    to_insert_end = to_insert.lower_bound(*(file_it.upper_bound()));
  for (auto it(to_insert_begin); it != to_insert_end; ++it)
  {
    auto ic_it(index_values.find(it->first));
    if (ic_it == index_values.end())
    {
      index_values.insert(std::make_pair(it->first,
          Index_Collection< TIndex, TObject, TContainer >(nullptr, nullptr, to_delete.end(), it)));
    }
    else
      ic_it->second.insert_it = it;
  }

  // compute the distribution over different blocks
  // and log all objects that will be deleted
  for (typename std::map< TIndex, Index_Collection< TIndex, TObject, TContainer > >::const_iterator
      it(index_values.begin()); it != index_values.end(); ++it)
  {
    uint32 current_size(0);

    if (it->second.source_begin != nullptr)
    {
      uint8* pos(it->second.source_begin + 4);
      pos = pos + TIndex::size_of((it->second.source_begin) + 4);
      while (pos < it->second.source_end)
      {
        TObject obj(pos);
        if ((it->second.delete_it == to_delete.end()) ||
          (find_elem(it->second.delete_it->second, obj) == it->second.delete_it->second.end()))
          current_size += obj.size_of();

        pos = pos + obj.size_of();
      }
      if (current_size > 0)
        current_size += TIndex::size_of((it->second.source_begin) + 4) + 4;
    }

    if ((it->second.insert_it != to_insert.end()) &&
      (!(it->second.insert_it->second.empty())))
    {
      // only add nonempty indices
      if (current_size == 0)
        current_size += it->first.size_of() + 4;
      for (auto it2(it->second.insert_it->second.begin());
      it2 != it->second.insert_it->second.end(); ++it2)
        current_size += it2->size_of();
    }

    sizes[it->first] += current_size;
    vsizes.push_back(current_size);
  }

  std::set< TIndex > index_values_set;
  for (typename std::map< TIndex, Index_Collection< TIndex, TObject, TContainer > >::const_iterator
      it(index_values.begin()); it != index_values.end(); ++it)
    index_values_set.insert(it->first);
  calc_split_idxs(split, vsizes, index_values_set.begin(), index_values_set.end());

  // really write data
  typename std::vector< TIndex >::const_iterator split_it(split.begin());
  pos = (dest.get() + 4);
  uint32 max_size = 0;
  for (typename std::map< TIndex, Index_Collection< TIndex, TObject, TContainer > >::const_iterator
    it(index_values.begin()); it != index_values.end(); ++it)
  {
    if ((split_it != split.end()) && (it->first == *split_it))
    {
      *(uint32*)dest.get() = pos - dest.get();
      if (pos - dest.get() > 8)
        file_it = file_blocks.insert_block(file_it, (uint64*)dest.get(), max_size);
      ++split_it;
      pos = dest.get() + 4;
      max_size = 0;
    }

    if (sizes[it->first] > max_size)
      max_size = sizes[it->first];

    if (sizes[it->first] == 0)
      continue;
    else if (sizes[it->first] < block_size - 4)
    {
      uint8* current_pos(pos);
      it->first.to_data(pos + 4);
      pos += it->first.size_of() + 4;

      if (it->second.source_begin != nullptr)
      {
        uint8* spos(it->second.source_begin + 4);
        spos = spos + TIndex::size_of((it->second.source_begin) + 4);
        while (spos < it->second.source_end)
        {
          TObject obj(spos);
          if ((it->second.delete_it == to_delete.end()) ||
            (find_elem(it->second.delete_it->second, obj) == it->second.delete_it->second.end()))
          {
            memcpy(pos, spos, obj.size_of());
            pos = pos + obj.size_of();
          }
          spos = spos + obj.size_of();
        }
      }

      if ((it->second.insert_it != to_insert.end()) &&
        (!(it->second.insert_it->second.empty())))
      {
        // only add nonempty indices
        for (auto it2(it->second.insert_it->second.begin());
        it2 != it->second.insert_it->second.end(); ++it2)
        {
          it2->to_data(pos);
          pos = pos + it2->size_of();
        }
      }
      unalignedStore(current_pos, (uint32)(pos - dest.get()));
    }
    else
    {
      it->first.to_data(pos + 4);
      pos += it->first.size_of() + 4;

      // can never overflow - we have read only one block
      if (it->second.source_begin != nullptr)
      {
        uint8* spos(it->second.source_begin + 4);
        spos = spos + TIndex::size_of((it->second.source_begin) + 4);
        while (spos < it->second.source_end)
        {
          TObject obj(spos);
          if ((it->second.delete_it == to_delete.end()) ||
            (find_elem(it->second.delete_it->second, obj) == it->second.delete_it->second.end()))
          {
            memcpy(pos, spos, obj.size_of());
            pos = pos + obj.size_of();
          }
          spos = spos + obj.size_of();
        }
      }

      if ((it->second.insert_it != to_insert.end()) &&
        (!(it->second.insert_it->second.empty())))
      {
        for (auto it2(it->second.insert_it->second.begin());
            it2 != it->second.insert_it->second.end(); ++it2)
          flush_if_necessary_and_write_obj((uint64*)dest.get(), pos, file_it, it->first, *it2);
      }

      if ((uint32)(pos - dest.get()) == it->first.size_of() + 8)
        // the block is in fact empty
        pos = dest.get() + 4;

      unalignedStore(dest.get() + 4, (uint32)(pos - dest.get()));
      max_size = pos - dest.get() - 4;
    }
  }

  if (pos > dest.get() + 4)
  {
    *(uint32*)dest.get() = pos - dest.get();
    file_it = file_blocks.replace_block(file_it, (uint64*)dest.get(), max_size);
    ++file_it;
  }
  else
    file_it = file_blocks.erase_block(file_it);
}


template< class TIndex, class TObject, class TIterator >
void Block_Backend_Updater< TIndex, TObject, TIterator >::flush_or_delete_block(
    uint64* start_ptr, uint bytes_written, typename File_Blocks_::Write_Iterator& file_it, uint32 idx_size)
{
  if (bytes_written > 8 + idx_size)
  {
    unalignedStore(start_ptr, (uint32) bytes_written);
    unalignedStore(((uint32*)start_ptr) + 1, (uint32) bytes_written);
    file_it = file_blocks.replace_block(file_it, start_ptr, bytes_written - 4);
    ++file_it;
  }
  else
    file_it = file_blocks.erase_block(file_it);
}



template< class TIndex, class TObject, class TIterator >
bool Block_Backend_Updater< TIndex, TObject, TIterator >::read_block_or_blocks(
    typename File_Blocks_::Write_Iterator& file_it, std::unique_ptr< uint64[] >& source, uint32& buffer_size)
{
  file_blocks.read_block(file_it, source.get());

  if (*(((uint32*)source.get()) + 1) > block_size)
  {
    uint32 new_buffer_size = (*(((uint32*)source.get()) + 1)/block_size + 1) * block_size;
    if (buffer_size < new_buffer_size)
    {
      std::unique_ptr< uint64[] > new_source_buffer(new uint64[new_buffer_size / 8]);
      memcpy(new_source_buffer.get(), source.get(), block_size);
      source.swap(new_source_buffer);

      buffer_size = new_buffer_size;
    }
    for (uint i_offset = block_size; i_offset < new_buffer_size; i_offset += block_size)
    {
      ++file_it;
      file_blocks.read_block(file_it, source.get() + i_offset/8, false);
    }

    return true;
  }
  return false;
}


template< class TIndex, class TObject, class TIterator >
template< template<class...> class TContainer >
uint32 Block_Backend_Updater< TIndex, TObject, TIterator >::skip_deleted_objects(
    uint64* source_start_ptr, uint64* dest_start_ptr,
    const TContainer< TObject >& objs_to_delete, uint32 idx_size,
    const TIndex& idx)
{
  uint32 src_obj_offset = 8 + idx_size;
  uint32 dest_obj_offset = src_obj_offset;
  uint32 src_size = *(uint32*)source_start_ptr;
  memcpy(((uint8*)dest_start_ptr) + 8, ((uint8*)source_start_ptr) + 8, idx_size);

  while (src_obj_offset < src_size)
  {
    TObject obj(((uint8*)source_start_ptr) + src_obj_offset);
    if (find_elem(objs_to_delete, obj) == objs_to_delete.end())
    {
      memcpy(
          ((uint8*)dest_start_ptr) + dest_obj_offset, ((uint8*)source_start_ptr) + src_obj_offset,
          obj.size_of());
      dest_obj_offset += obj.size_of();
    }

    src_obj_offset += obj.size_of();
  }

  *(uint32*)dest_start_ptr = dest_obj_offset;
  *(((uint32*)dest_start_ptr)+1) = dest_obj_offset;
  memcpy(dest_start_ptr + 1, source_start_ptr + 1, idx_size);

  return src_obj_offset == dest_obj_offset ? 0 : dest_obj_offset;
}


template< typename Object, template<class...> class TContainer >
void append_insertables(
    uint64* dest_start_ptr, uint32 block_size,
    typename TContainer< Object >::const_iterator& cur_insert,
    const typename TContainer< Object >::const_iterator& cur_end)
{
  uint32 obj_append_offset = *(uint32*)dest_start_ptr;

  while ((cur_insert != cur_end) && (obj_append_offset + cur_insert->size_of() < block_size))
  {
    cur_insert->to_data(((uint8*)dest_start_ptr) + obj_append_offset);
    obj_append_offset += cur_insert->size_of();
    ++cur_insert;
  }

  *(uint32*)dest_start_ptr = obj_append_offset;
  *(((uint32*)dest_start_ptr)+1) = obj_append_offset;
}

template <class T>
auto find_elem(const std::set<T> & container, const T& elem)
{
  return container.find(elem);
}

template <class T>
auto find_elem(const std::vector<T> & container, const T& elem)
{
  auto lower = std::lower_bound(container.begin(), container.end(), elem);
  if (lower != container.end() && !(*lower == elem))
    return container.end();
  return lower;
}



template< class TIndex, class TObject, class TIterator >
template< template<class...> class TContainer >
void Block_Backend_Updater< TIndex, TObject, TIterator >::update_segments
      (typename File_Blocks_::Write_Iterator& file_it,
       const std::map< TIndex, TContainer< TObject > >& to_delete,
       const std::map< TIndex, TContainer< TObject > >& to_insert)
{
  file_it.start_segments_mode();
  uint32 buffer_size = block_size;
  std::unique_ptr< uint64[] > source(new uint64[buffer_size / 8]);
  std::unique_ptr< uint64[] > dest(new uint64[buffer_size / 8]);
  TIndex idx = file_it.block().index;
  auto delete_it(to_delete.find(idx));
  auto insert_it(to_insert.find(idx));
  uint32 idx_size = idx.size_of();

  typename TContainer< TObject >::const_iterator cur_insert;
  if (insert_it != to_insert.end())
    cur_insert = insert_it->second.begin();

  while (!file_it.is_end() && file_it.block().index == idx)
  {
    typename File_Blocks_::Write_Iterator delta_it = file_it;
    bool oversized = read_block_or_blocks(file_it, source, buffer_size);
    if (oversized)
    {
      ++file_it;
      TObject obj(((uint8*)source.get()) + 8 + idx_size);
      if (delete_it != to_delete.end() && find_elem(delete_it->second, obj) != delete_it->second.end())
        file_blocks.erase_blocks(delta_it, file_it);
    }
    else
    {
      if (*(uint32*)source.get() < 8 + idx_size)
      { // something has seriously gone wrong - such a block shuld not exist
        ++file_it;
        continue;
      }

      uint32 obj_append_offset = 0;
      if (delete_it != to_delete.end())
        obj_append_offset = skip_deleted_objects(
            source.get(), dest.get(), delete_it->second, idx_size, idx);
      else
        memcpy(dest.get(), source.get(), *(uint32*)source.get());

      if (obj_append_offset)
      {
        if (insert_it != to_insert.end())
          append_insertables< TObject, TContainer >(dest.get(), block_size, cur_insert, insert_it->second.end());
        flush_or_delete_block(dest.get(), *(uint32*)dest.get(), file_it, idx_size);
      }
      else if (insert_it != to_insert.end() && *(uint32*)source.get() < block_size/2)
      {
        append_insertables< TObject, TContainer >(dest.get(), block_size, cur_insert, insert_it->second.end());
        flush_or_delete_block(dest.get(), *(uint32*)dest.get(), file_it, idx_size);
      }
      else
        ++file_it;
    }
  }

  uint8* pos = ((uint8*)dest.get()) + idx_size + 8;
  memcpy(dest.get()+1, source.get()+1, idx_size);
  if (insert_it != to_insert.end())
  {
    while (cur_insert != insert_it->second.end())
    {
      flush_if_necessary_and_write_obj(dest.get(), pos, file_it, idx, *cur_insert);
      ++cur_insert;
    }
  }
  if (pos > ((uint8*)dest.get()) + idx_size + 8)
  {
    *(uint32*)dest.get() = pos - (uint8*)dest.get();
    *(((uint32*)dest.get())+1) = pos - (uint8*)dest.get();
    file_it = file_blocks.insert_block(file_it, dest.get(), pos - (uint8*)dest.get() - 4);
  }

  file_it.end_segments_mode();
}


#endif
