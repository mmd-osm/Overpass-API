/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__DISPATCH__RESOURCE_MANAGER_H
#define DE__OSM3S___OVERPASS_API__DISPATCH__RESOURCE_MANAGER_H

#include <ctime>
#include "../../template_db/transaction.h"
#include "../core/datatypes.h"
#include "../core/parsed_query.h"
#include "../data/diff_set.h"
#include "../data/user_data_cache.h"


class Statement;


namespace Diff_Action
{
  enum _ { positive, collect_lhs, collect_rhs_no_del, collect_rhs_with_del, show_old, show_new };
}


class Runtime_Stack_Frame
{
public:
  Runtime_Stack_Frame(Runtime_Stack_Frame* parent_ = nullptr)
    : parent(parent_), loop_count(0), loop_size(0),
    desired_timestamp(parent_ ? parent_->desired_timestamp : NOW),
    desired_action(parent_ ? parent_->desired_action : Diff_Action::positive),
    diff_from_timestamp(parent_ ? parent_->diff_from_timestamp : NOW),
    diff_to_timestamp(parent_ ? parent_->diff_to_timestamp : NOW) {}

  // Returns the used RAM including the used RAM of parent frames
  uint64 total_used_space() const;

  Set* get_set(const std::string& set_name);
  Diff_Set* get_diff_set(const std::string& set_name);
  const std::string* get_value(const std::string& set_name, const std::string& key);
  const std::map< std::string, std::string >* get_set_key_values(const std::string& set_name);
  void swap_set(const std::string& set_name, Set& set_);
  void swap_diff_set(const std::string& set_name, Diff_Set& set_);
  void set_value(const std::string& set_name, const std::string& key, const std::string& value);
  void erase_set(const std::string& set_name);
  void clear_sets();

  void copy_outward(const std::string& inner_set_name, const std::string& top_set_name);
  void move_outward(const std::string& inner_set_name, const std::string& top_set_name);
  bool union_inward(const std::string& top_set_name, const std::string& inner_set_name);
  void union_current_frame(const std::string& top_set_name, const std::string& inner_set_name);
  void copy_inward(const std::string& top_set_name, const std::string& inner_set_name);
  void substract_from_inward(const std::string& top_set_name, const std::string& inner_set_name);
  void move_all_inward();
  void move_all_inward_except(const std::string& set_name);

  bool set_exists_in_parents(const std::string& inner_set_name) const;

  timestamp_t get_desired_timestamp() const { return desired_timestamp; }
  Diff_Action::_ get_desired_action() const { return desired_action; }
  timestamp_t get_diff_from_timestamp() const { return diff_from_timestamp; }
  timestamp_t get_diff_to_timestamp() const { return diff_to_timestamp; }

  void set_desired_timestamp(timestamp_t timestamp) { desired_timestamp = (timestamp == 0 ? NOW : timestamp); }
  void set_desired_action(Diff_Action::_ action) { desired_action = action; }
  void set_diff_from_timestamp(timestamp_t timestamp) { diff_from_timestamp = timestamp; }
  void set_diff_to_timestamp(timestamp_t timestamp) { diff_to_timestamp = timestamp; }

  uint64 total_size();
  std::vector< std::pair< uint, uint > > stack_progress() const;
  void set_loop_size(uint loop_size_) { loop_size = loop_size_; }
  void count_loop() { ++loop_count; }

private:
  Runtime_Stack_Frame* parent;
  std::map< std::string, Set > sets;
  std::map< std::string, Diff_Set > diff_sets;
  std::map< std::string, std::map< std::string, std::string > > key_values;
  std::map< std::string, uint64 > size_per_set;
  uint loop_count;
  uint loop_size;

  timestamp_t desired_timestamp;
  Diff_Action::_ desired_action;
  timestamp_t diff_from_timestamp;
  timestamp_t diff_to_timestamp;
};

namespace {

template< typename Object >
inline void clear_object(Set* s) = delete;


template< > inline void clear_object< Way_Skeleton >(Set* s) {
  auto empty_ways = std::map< Uint31_Index, std::vector< Way_Skeleton > >();
  s->ways.swap(empty_ways);
}

}

class Resource_Manager
{
public:
  Resource_Manager(Transaction& transaction_, Parsed_Query* global_settings_ = nullptr,
		   Error_Output* error_output_ = nullptr);

  Resource_Manager(Transaction& transaction_, Parsed_Query& global_settings_, Error_Output* error_output_,
		   Transaction& area_transaction_,
		   Area_Usage_Listener* area_updater__);

  Resource_Manager(const Resource_Manager&) = delete;
  Resource_Manager& operator=(const Resource_Manager& a) = delete;


  ~Resource_Manager()
  {
    for (auto it = runtime_stack.begin();
        it != runtime_stack.end(); ++it)
      delete *it;

    if (global_settings_owned)
      delete global_settings;
    delete area_updater_;
  }

  const Set* get_set(const std::string& set_name) const;
  const Diff_Set* get_diff_set(const std::string& set_name);
  const std::string* get_value(const std::string& set_name, const std::string& key);
  const std::map< std::string, std::string >* get_set_key_values(const std::string& set_name);
  void swap_set(const std::string& set_name, Set& set_);
  void swap_diff_set(const std::string& set_name, Diff_Set& set_);
  void set_value(const std::string& set_name, const std::string& key, const std::string& value);
  void erase_set(const std::string& set_name);
  void clear_sets();

  void push_stack_frame();
  void copy_outward(const std::string& inner_set_name, const std::string& top_set_name);
  void move_outward(const std::string& inner_set_name, const std::string& top_set_name);
  void union_current_frame(const std::string& top_set_name, const std::string& inner_set_name);
  bool union_inward(const std::string& top_set_name, const std::string& inner_set_name);
  void copy_inward(const std::string& top_set_name, const std::string& inner_set_name);
  void substract_from_inward(const std::string& top_set_name, const std::string& inner_set_name);
  void move_all_inward();
  void move_all_inward_except(const std::string& set_name);
  void pop_stack_frame();
  bool set_exists_in_parents(const std::string& set_name);

  template <typename T>
  void clear_object_in_set(const std::string& set_name) {
    if (runtime_stack.empty())
      return;

    Set * s = runtime_stack.back()->get_set(set_name);
    if (s != nullptr)
      clear_object<T>(s);
  }

  void count_loop();

  Area_Usage_Listener* area_updater()
  {
    return area_updater_;
  }

  Parsed_Query& get_global_settings() const { return *global_settings; }

  void log_and_display_error(const std::string& message);

  bool health_check(const Statement& stmt, uint32 extra_time = 0, uint64 extra_space = 0);

  void set_limits(uint32 max_allowed_time_, uint64 max_allowed_space_)
  {
    max_allowed_time = max_allowed_time_;
    max_allowed_space = max_allowed_space_;
  }

  Transaction* get_transaction() { return transaction; }
  Transaction* get_area_transaction() { return area_transaction; }

  timestamp_t get_desired_timestamp() const;
  Diff_Action::_ get_desired_action() const;
  timestamp_t get_diff_from_timestamp() const;
  timestamp_t get_diff_to_timestamp() const;

  void set_desired_timestamp(timestamp_t timestamp);
  void start_diff(timestamp_t comparison_timestamp, timestamp_t desired_timestamp);
  void switch_diff_rhs(bool add_deletion_information);
  void switch_diff_show_from(const std::string& diff_set_name);
  void switch_diff_show_to(const std::string& diff_set_name);

  const user_id_name_t& users() { return user_data_cache.users(*transaction); }
  Tags_By_Id_Cache& tags_by_id() { return tags_by_id_cache; }

  void start_cpu_timer(uint index);
  void stop_cpu_timer(uint index);
  const std::vector< std::chrono::milliseconds >& cpu_time() const { return cpu_runtime; }

private:
  std::vector< Runtime_Stack_Frame* > runtime_stack;

  Transaction* transaction = nullptr;
  Error_Output* error_output = nullptr;
  Transaction* area_transaction = nullptr;
  Area_Usage_Listener* area_updater_ = nullptr;
  Parsed_Query* global_settings = nullptr;
  bool global_settings_owned;
  User_Data_Cache user_data_cache;
  Tags_By_Id_Cache tags_by_id_cache;        // global Tags_By_Id Cache, used by Tag_Store
  int start_time;
  uint32 last_ping_time;
  uint32 last_report_time;
  uint32 max_allowed_time;
  uint64 max_allowed_space;

  std::vector< std::chrono::time_point<std::chrono::system_clock> > cpu_start_time;
  std::vector< std::chrono::milliseconds > cpu_runtime;
};


template<typename T>
constexpr uint64 eval_elem() = delete;

template<>
constexpr uint64 eval_elem< Node_Skeleton >() { return 8; }

template<>
constexpr uint64 eval_elem< Way_Skeleton >() { return 128; }

template<>
constexpr uint64 eval_elem< Relation_Skeleton>() { return 192; }

template<>
constexpr uint64 eval_elem< Attic< Node_Skeleton > >() { return 16; }

template<>
constexpr uint64 eval_elem< Attic< Way_Skeleton > >() { return 136; }

template<>
constexpr uint64 eval_elem< Attic< Relation_Skeleton > >() { return 200; }

template<>
constexpr uint64 eval_elem< Area_Skeleton >() { return 128; }

template<>
constexpr uint64 eval_elem< Derived_Structure >() { return 128; }

template<>
constexpr uint64 eval_elem< Area_Block >() { return 128; }

const uint64 eval_map_index_size = 64;


template<class TIndex, class TObject>
uint64 eval_map(const std::map< TIndex, std::vector< TObject > >& obj) {
  uint64 size(0);
  for (auto it(obj.begin()); it != obj.end(); ++it)
    size += it->second.size()*eval_elem<TObject>() + eval_map_index_size;
  return size;
}


struct Resource_Error
{
  bool timed_out;
  std::string stmt_name;
  uint line_number;
  uint64 size;
  uint runtime;
};


struct Cpu_Timer
{
  Cpu_Timer(Resource_Manager& rman_, uint index_)
      : rman(&rman_), index(index_)
  { rman->start_cpu_timer(index); }
  ~Cpu_Timer() { rman->stop_cpu_timer(index); }

private:
  Resource_Manager* rman;
  uint index;
};


#endif
