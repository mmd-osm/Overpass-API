/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__CORE__TYPE_RELATION_H
#define DE__OSM3S___OVERPASS_API__CORE__TYPE_RELATION_H

#include "basic_types.h"
#include "index_computations.h"

#include <cstring>
#include <map>
#include <set>
#include <string>
#include <vector>


struct Relation_Entry
{
  typedef Global_Id_Type Ref_Type;

  Relation_Entry() noexcept = default;

  Global_Id_Type ref{};
  uint32 type{};
  uint32 role{};
  const static uint32 NODE = 1;
  const static uint32 WAY = 2;
  const static uint32 RELATION = 3;

  bool operator==(const Relation_Entry& a) const noexcept
  {
    return (a.ref == this->ref && a.type == this->type && a.role == this->role);
  }

  Uint32_Index ref32() const { return Uint32_Index(ref.val()); }
};


struct Relation
{
  typedef Uint32_Index Id_Type;

  Id_Type id{};
  uint32 index{};
  std::vector< Relation_Entry > members;
  std::vector< Uint31_Index > node_idxs;
  std::vector< Uint31_Index > way_idxs;
  std::vector< std::pair< std::string, std::string > > tags;

  Relation() noexcept = default;

  Relation(Id_Type id_) noexcept : id(id_) {}

  Relation(Id_Type id_, uint32 index_, const std::vector< Relation_Entry >& members_)
  : id(id_), index(index_), members(members_) {}

  static uint32 calc_index(const std::vector< uint32 >& memb_idxs)
  {
    return ::calc_index(memb_idxs);
  }

  static bool indicates_geometry(Uint31_Index index)
  {
    return ((index.val() & 0x80000000) != 0 && ((index.val() & 0x1) == 0));
  }
};


struct Relation_Comparator_By_Id {
  bool operator() (const Relation* a, const Relation* b) noexcept
  {
    return (a->id < b->id);
  }
};


struct Relation_Equal_Id {
  bool operator() (const Relation* a, const Relation* b) noexcept
  {
    return (a->id == b->id);
  }
};


struct Relation_Delta;

template <class T, class Object>
struct Relation_Skeleton_Handle_Methods;

class Relation_Skeleton_Data final : public SharedData
{
public:
  Relation_Skeleton_Data() = default;

  Relation_Skeleton_Data(const Relation_Skeleton_Data &other) = default;

  Relation_Skeleton_Data(const void* data)
  {
    const auto member_count = unalignedLoad<uint32>((uint32*)data + 1);
    const auto node_idxs_count = unalignedLoad<uint32>((uint32*)data + 2);
    const auto way_idxs_count = unalignedLoad<uint32>((uint32*)data + 3);

    members.resize(member_count);
    node_idxs.resize(node_idxs_count, 0u);
    way_idxs.resize(way_idxs_count, 0u);
    for (uint i(0); i < member_count; ++i)
    {
      members[i].ref = unalignedLoad<uint64>((uint32*)data + 4 + 3*i);
      members[i].role = unalignedLoad<uint32>((uint32*)data + 6 + 3*i) & 0xffffff;
      members[i].type = *((uint8*)data + 27 + 12*i);
    }
    uint32* start_ptr = (uint32*)data + 4 + 3* members.size();
    for (uint i = 0; i < node_idxs.size(); ++i)
      node_idxs[i] = *(start_ptr + i);
    start_ptr = (uint32*)data + 4 + 3* members.size() + node_idxs.size();
    for (uint i = 0; i < way_idxs.size(); ++i)
      way_idxs[i] = *(start_ptr + i);
  }

  ~Relation_Skeleton_Data() = default;

  std::vector< Relation_Entry > members;
  std::vector< Uint31_Index > node_idxs;
  std::vector< Uint31_Index > way_idxs;
};


struct
#ifdef HAVE_WORD_ALIGNMENT
__attribute__ ((packed, aligned(4)))
#endif
Relation_Skeleton
{
  typedef Relation::Id_Type Id_Type;
  typedef Relation_Delta Delta;

  Id_Type id{};

  Relation_Skeleton() : id(0u),  d(new Relation_Skeleton_Data) { }

  Relation_Skeleton(Relation::Id_Type id_) : id(id_), d(new Relation_Skeleton_Data) { }

  Relation_Skeleton(const void* data) : id(unalignedLoad<Id_Type>(data)), d(new Relation_Skeleton_Data(data)) {}

  Relation_Skeleton(const Relation& rel)
      : id(rel.id), d(new Relation_Skeleton_Data) {

    d->members = rel.members;
    d->node_idxs = rel.node_idxs;
    d->way_idxs = rel.way_idxs;

  }

  Relation_Skeleton(Id_Type id_, std::vector< Relation_Entry >&& members_)
      : id(id_), d(new Relation_Skeleton_Data) {

    d->members = std::move(members_);
  }

  Relation_Skeleton(Id_Type id_, const std::vector< Relation_Entry >& members_,
		    const std::vector< Uint31_Index >& node_idxs_,
		    const std::vector< Uint31_Index >& way_idxs_)
      : id(id_), d(new Relation_Skeleton_Data) {

    d->members = members_;
    d->node_idxs = node_idxs_;
    d->way_idxs = way_idxs_;
  }

  Relation_Skeleton(Id_Type id_,  std::vector< Relation_Entry > && members_,
                     std::vector< Uint31_Index > && node_idxs_,
                     std::vector< Uint31_Index > && way_idxs_)
      : id(id_), d(new Relation_Skeleton_Data) {

    d->members = std::move(members_);
    d->node_idxs = std::move(node_idxs_);
    d->way_idxs = std::move(way_idxs_);
  }

  uint32 size_of() const noexcept
  {
    return 16 + 12* d->members.size() + 4* d->node_idxs.size() + 4* d->way_idxs.size();
  }

  static uint32 size_of(const void* data)
  {
    return 16 + 12 * *((uint32*)data + 1) + 4* *((uint32*)data + 2) + 4* *((uint32*)data + 3);
  }

  void to_data(void* data) const noexcept
  {
    unalignedStore(data, id.val());
    unalignedStore(((uint32*)data + 1), (uint32) d->members.size());
    unalignedStore(((uint32*)data + 2), (uint32) d->node_idxs.size());
    unalignedStore(((uint32*)data + 3), (uint32) d->way_idxs.size());
    for (uint i = 0; i < d->members.size(); ++i)
    {
      unalignedStore( ((uint32*)data + 4 + 3*i), (uint64) d->members[i].ref.val());
      unalignedStore( ((uint32*)data + 6 + 3*i), (uint32)(d->members[i].role & 0xffffff));
      *((uint8*)data + 27 + 12*i) = d->members[i].type;
    }
    Uint31_Index* start_ptr = (Uint31_Index*)data + 4 + 3* d->members.size();
    for (uint i = 0; i < d->node_idxs.size(); ++i)
      *(start_ptr + i) = d->node_idxs[i];
    start_ptr = (Uint31_Index*)data + 4 + 3* d->members.size() + d->node_idxs.size();
    for (uint i = 0; i < d->way_idxs.size(); ++i)
      *(start_ptr + i) = d->way_idxs[i];
  }

  const std::vector< Relation_Entry > & members() const { return d->members; }
  const std::vector< Relation_Entry > & c_members() const { return d->members; }
  std::vector< Relation_Entry > & members() { return d->members; }

  const std::vector< Uint31_Index > & node_idxs() const { return d->node_idxs; }
  const std::vector< Uint31_Index > & c_node_idxs() const { return d->node_idxs; }
  std::vector< Uint31_Index > & node_idxs() { return d->node_idxs; }

  const std::vector< Uint31_Index > & way_idxs() const { return d->way_idxs; }
  const std::vector< Uint31_Index > & c_way_idxs() const { return d->way_idxs; }
  std::vector< Uint31_Index > & way_idxs() { return d->way_idxs; }


  bool operator<(const Relation_Skeleton& a) const noexcept
  {
    return this->id < a.id;
  }

  bool operator==(const Relation_Skeleton& a) const noexcept
  {
    return this->id == a.id;
  }

  template <class T, class Object>
  using Handle_Methods = Relation_Skeleton_Handle_Methods<T, Object>;

private:
  SharedDataPointer<Relation_Skeleton_Data> d;
};




template <class T, class Object>
struct Relation_Skeleton_Handle_Methods
{
  typename Object::Id_Type inline id() const {
     return (static_cast<const T*>(this)->apply_func(Relation_Skeleton_Id_Functor<typename Object::Id_Type>()));
  }

  void inline add_element(std::vector< Object > & v) const {
    static_cast<const T*>(this)->apply_func(Generic_Add_Element_Functor<Object>(v));
  }

  bool inline has_child_with_id(const std::vector< Global_Id_Type >& ids, uint32 type) const {
    return (static_cast<const T*>(this)->apply_func(Relation_Skeleton_Has_Child_with_Id_Functor(ids, type)));
  }

  uint32 inline get_member_count() const {
    return (static_cast<const T*>(this)->apply_func(Relation_Skeleton_Member_Count_Functor()));
  }

private:
  template <typename Id_Type >
  struct Relation_Skeleton_Id_Functor {
    Relation_Skeleton_Id_Functor() = default;

    using reference_type = Relation_Skeleton;

    Id_Type operator()(const void* data) const
     {
       return unalignedLoad<Id_Type>(data);
     }
  };

  struct Relation_Skeleton_Has_Child_with_Id_Functor {
    Relation_Skeleton_Has_Child_with_Id_Functor(const std::vector< Global_Id_Type >& ids, uint32 type) : ids(ids), type(type) {};

    using reference_type = Relation_Skeleton;

    bool operator()(const void* data) const
    {
      const auto member_count = unalignedLoad<uint32>((uint32*)data + 1);

      for (uint i(0); i < member_count; ++i)
      {
        const uint32 member_type = *((uint8*)data + 27 + 12*i);

        if (member_type != type)
          continue;

        const Global_Id_Type member_ref = unalignedLoad<uint64>((uint32*)data + 4 + 3*i);
        if (std::binary_search(ids.begin(), ids.end(), member_ref))
          return true;
      }
      return false;
    }

  private:
    const std::vector< Global_Id_Type >& ids;
    const uint32 type;
  };

  struct Relation_Skeleton_Member_Count_Functor {
    Relation_Skeleton_Member_Count_Functor() = default;

    using reference_type = Relation_Skeleton;

    uint32 operator()(const void* data) const
     {
       return unalignedLoad<uint32>((uint32*)data + 1);
     }
  };
};

inline std::ostream & operator<<(std::ostream &os, const Relation_Skeleton & p)
{
  return os << "Rel(" << p.id << ", " << p.members().size() << " mbr)";
}

template <class T, class Object>
struct Relation_Delta_Handle_Methods;

struct Relation_Delta
{
  typedef Relation_Skeleton::Id_Type Id_Type;

  Id_Type id{};
  bool full{};
  std::vector< uint > members_removed;
  std::vector< std::pair< uint, Relation_Entry > > members_added;
  std::vector< uint > node_idxs_removed;
  std::vector< std::pair< uint, Uint31_Index > > node_idxs_added;
  std::vector< uint > way_idxs_removed;
  std::vector< std::pair< uint, Uint31_Index > > way_idxs_added;

  Relation_Delta() noexcept = default;

  Relation_Delta(const void* data) : id(unalignedLoad<Id_Type>(data)), full(false)
  {
    if (unalignedLoad<uint32>((uint32*)data + 1) == 0xffffffff)
    {
      full = true;
      members_removed.clear();
      members_added.resize(unalignedLoad<uint32>((uint32*)data + 2));
      node_idxs_removed.clear();
      node_idxs_added.resize(unalignedLoad<uint32>((uint32*)data + 3), std::make_pair(0, 0u));
      way_idxs_removed.clear();
      way_idxs_added.resize(unalignedLoad<uint32>((uint32*)data + 4), std::make_pair(0, 0u));

      uint8* ptr = ((uint8*)data) + 20;

      for (uint i(0); i < members_added.size(); ++i)
      {
        members_added[i].first = i;
        members_added[i].second.ref = unalignedLoad<uint64>(ptr);
        members_added[i].second.role = unalignedLoad<uint32>(ptr + 8) & 0xffffff;
        members_added[i].second.type = *((uint8*)(ptr + 11));
        ptr += 12;
      }

      for (uint i = 0; i < node_idxs_added.size(); ++i)
      {
        node_idxs_added[i].first = i;
        node_idxs_added[i].second = unalignedLoad<uint32>(ptr);
        ptr += 4;
      }

      for (uint i = 0; i < way_idxs_added.size(); ++i)
      {
        way_idxs_added[i].first = i;
        way_idxs_added[i].second = unalignedLoad<uint32>(ptr);
        ptr += 4;
      }
    }
    else
    {
      members_removed.resize(unalignedLoad<uint32>((uint32*)data + 1));
      members_added.resize(unalignedLoad<uint32>((uint32*)data + 2));
      node_idxs_removed.resize(unalignedLoad<uint32>((uint32*)data + 3));
      node_idxs_added.resize(unalignedLoad<uint32>((uint32*)data + 4), std::make_pair(0, 0u));
      way_idxs_removed.resize(unalignedLoad<uint32>((uint32*)data + 5));
      way_idxs_added.resize(unalignedLoad<uint32>((uint32*)data + 6), std::make_pair(0, 0u));

      uint8* ptr = ((uint8*)data) + 28;

      for (uint i(0); i < members_removed.size(); ++i)
      {
        members_removed[i] = unalignedLoad<uint32>(ptr);
        ptr += 4;
      }

      for (uint i(0); i < members_added.size(); ++i)
      {
        members_added[i].first = unalignedLoad<uint32>(ptr);
        members_added[i].second.ref = unalignedLoad<uint64>(ptr + 4);
        members_added[i].second.role = unalignedLoad<uint32>(ptr + 12) & 0xffffff;
        members_added[i].second.type = *((uint8*)(ptr + 15));
        ptr += 16;
      }

      for (uint i = 0; i < node_idxs_removed.size(); ++i)
      {
        node_idxs_removed[i] = unalignedLoad<uint32>(ptr);
        ptr += 4;
      }

      for (uint i = 0; i < node_idxs_added.size(); ++i)
      {
        node_idxs_added[i].first = unalignedLoad<uint32>(ptr);
        node_idxs_added[i].second = unalignedLoad<uint32>(ptr + 4);
        ptr += 8;
      }

      for (uint i = 0; i < way_idxs_removed.size(); ++i)
      {
        way_idxs_removed[i] = unalignedLoad<uint32>(ptr);
        ptr += 4;
      }

      for (uint i = 0; i < way_idxs_added.size(); ++i)
      {
        way_idxs_added[i].first = unalignedLoad<uint32>(ptr);
        way_idxs_added[i].second = unalignedLoad<uint32>(ptr + 4);
        ptr += 8;
      }
    }
  }

  Relation_Delta(const Relation_Skeleton& reference, const Relation_Skeleton& skel)
    : id(skel.id), full(false)
  {
    if (!(id == skel.id))
      full = true;
    else
    {
      make_delta(skel.members(), reference.members(), members_removed, members_added);
      make_delta(skel.node_idxs(), reference.node_idxs(), node_idxs_removed, node_idxs_added);
      make_delta(skel.way_idxs(), reference.way_idxs(), way_idxs_removed, way_idxs_added);
    }

    if (members_added.size() >= skel.members().size()/2)
    {
      members_removed.clear();
      members_added.clear();
      node_idxs_removed.clear();
      node_idxs_added.clear();
      way_idxs_removed.clear();
      way_idxs_added.clear();
      full = true;
    }

    if (full)
    {
      copy_elems(skel.members(), members_added);
      copy_elems(skel.node_idxs(), node_idxs_added);
      copy_elems(skel.way_idxs(), way_idxs_added);
    }
  }

  Relation_Skeleton expand(const Relation_Skeleton& reference) const
  {
    if (full)
    {
      std::vector< Relation_Entry > members_;
      std::vector< Uint31_Index > node_idxs_;
      std::vector< Uint31_Index > way_idxs_;

      members_.reserve(members_added.size());
      for (uint i = 0; i < members_added.size(); ++i)
        members_.push_back(members_added[i].second);

      node_idxs_.reserve(node_idxs_added.size());
      for (uint i = 0; i < node_idxs_added.size(); ++i)
        node_idxs_.push_back(node_idxs_added[i].second);

      way_idxs_.reserve(way_idxs_added.size());
      for (uint i = 0; i < way_idxs_added.size(); ++i)
        way_idxs_.push_back(way_idxs_added[i].second);

      return Relation_Skeleton(id, std::move(members_), std::move(node_idxs_), std::move(way_idxs_));
    }
    else if (reference.id == id)
    {
      std::vector< Relation_Entry > members_;
      std::vector< Uint31_Index > node_idxs_;
      std::vector< Uint31_Index > way_idxs_;

      expand_diff(reference.members(), members_removed, members_added, members_);
      expand_diff(reference.node_idxs(), node_idxs_removed, node_idxs_added, node_idxs_);
      expand_diff(reference.way_idxs(), way_idxs_removed, way_idxs_added, way_idxs_);

      return Relation_Skeleton(id, std::move(members_), std::move(node_idxs_), std::move(way_idxs_));
    }

    return {};
  }

  Relation_Skeleton expand_fast(Relation_Skeleton& reference) const
  {
    if (full)
    {
      std::vector< Relation_Entry > members_;
      std::vector< Uint31_Index > node_idxs_;
      std::vector< Uint31_Index > way_idxs_;

      members_.reserve(members_added.size());
      for (uint i = 0; i < members_added.size(); ++i)
        members_.push_back(members_added[i].second);

      node_idxs_.reserve(node_idxs_added.size());
      for (uint i = 0; i < node_idxs_added.size(); ++i)
        node_idxs_.push_back(node_idxs_added[i].second);

      way_idxs_.reserve(way_idxs_added.size());
      for (uint i = 0; i < way_idxs_added.size(); ++i)
        way_idxs_.push_back(way_idxs_added[i].second);

      return Relation_Skeleton(id, std::move(members_), std::move(node_idxs_), std::move(way_idxs_));
    }
    else if (reference.id == id)
    {
      std::vector< Relation_Entry > members_;
      std::vector< Uint31_Index > node_idxs_;
      std::vector< Uint31_Index > way_idxs_;

      expand_diff_fast(reference.members(), members_removed, members_added, members_);
      expand_diff_fast(reference.node_idxs(), node_idxs_removed, node_idxs_added, node_idxs_);
      expand_diff_fast(reference.way_idxs(), way_idxs_removed, way_idxs_added, way_idxs_);

      return Relation_Skeleton(id, std::move(members_), std::move(node_idxs_), std::move(way_idxs_));
    }

    return {};
  }

  uint32 size_of() const noexcept
  {
    if (full)
      return 20 + 12*members_added.size() + 4*node_idxs_added.size() + 4*way_idxs_added.size();
    else
      return 28 + 4*members_removed.size() + 16*members_added.size()
          + 4*node_idxs_removed.size() + 8*node_idxs_added.size()
          + 4*way_idxs_removed.size() + 8*way_idxs_added.size();
  }

  static uint32 size_of(const void* data) noexcept
  {
    if (unalignedLoad<uint32>((uint32*)data + 1) == 0xffffffff)
      return 20 + 12 * unalignedLoad<uint32>((uint32*)data + 2) +
                   4 * unalignedLoad<uint32>((uint32*)data + 3) +
                   4 * unalignedLoad<uint32>((uint32*)data + 4);
    else
      return 28 + 4 * unalignedLoad<uint32>((uint32*)data + 1) +
                 16 * unalignedLoad<uint32>((uint32*)data + 2) +
                  4 * unalignedLoad<uint32>((uint32*)data + 3) +
                  8 * unalignedLoad<uint32>((uint32*)data + 4) +
                  4 * unalignedLoad<uint32>((uint32*)data + 5) +
                  8 * unalignedLoad<uint32>((uint32*)data + 6);
  }

  void to_data(void* data) const noexcept
  {
    unalignedStore(data, id.val());
    if (full)
    {
      unalignedStore(((uint32*)data + 1), (uint32) 0xffffffff);
      unalignedStore(((uint32*)data + 2), (uint32) members_added.size());
      unalignedStore(((uint32*)data + 3), (uint32) node_idxs_added.size());
      unalignedStore(((uint32*)data + 4), (uint32) way_idxs_added.size());

      uint8* ptr = ((uint8*)data) + 20;

      for (uint i = 0; i < members_added.size(); ++i)
      {
        unalignedStore(ptr, (uint64) members_added[i].second.ref.val());
        unalignedStore(ptr + 8, (uint32) members_added[i].second.role & 0xffffff);
        *((uint8*)(ptr + 11)) = members_added[i].second.type;
        ptr += 12;
      }

      for (uint i = 0; i < node_idxs_added.size(); ++i)
      {
        unalignedStore(ptr, node_idxs_added[i].second);
        ptr += 4;
      }

      for (uint i = 0; i < way_idxs_added.size(); ++i)
      {
        unalignedStore(ptr, way_idxs_added[i].second);
        ptr += 4;
      }
    }
    else
    {
      unalignedStore(((uint32*)data + 1), (uint32) members_removed.size());
      unalignedStore(((uint32*)data + 2), (uint32) members_added.size());
      unalignedStore(((uint32*)data + 3), (uint32) node_idxs_removed.size());
      unalignedStore(((uint32*)data + 4), (uint32) node_idxs_added.size());
      unalignedStore(((uint32*)data + 5), (uint32) way_idxs_removed.size());
      unalignedStore(((uint32*)data + 6), (uint32) way_idxs_added.size());

      uint8* ptr = ((uint8*)data) + 28;

      for (uint i = 0; i < members_removed.size(); ++i)
      {
        unalignedStore(ptr, (uint32) members_removed[i]);
        ptr += 4;
      }

      for (uint i = 0; i < members_added.size(); ++i)
      {
        unalignedStore(ptr, (uint32) members_added[i].first);
        unalignedStore(((uint32*)(ptr + 4)),  (uint64) members_added[i].second.ref.val());
        unalignedStore(((uint32*)(ptr + 12)), (uint32) members_added[i].second.role & 0xffffff);
        *((uint8*)(ptr + 15)) = members_added[i].second.type;
        ptr += 16;
      }

      for (uint i = 0; i < node_idxs_removed.size(); ++i)
      {
        unalignedStore(ptr, (uint32) node_idxs_removed[i]);
        ptr += 4;
      }

      for (uint i = 0; i < node_idxs_added.size(); ++i)
      {
        unalignedStore(ptr, (uint32) node_idxs_added[i].first);
        unalignedStore(ptr + 4, node_idxs_added[i].second);
        ptr += 8;
      }

      for (uint i = 0; i < way_idxs_removed.size(); ++i)
      {
        unalignedStore(ptr, (uint32) way_idxs_removed[i]);
        ptr += 4;
      }

      for (uint i = 0; i < way_idxs_added.size(); ++i)
      {
        unalignedStore(ptr, (uint32) way_idxs_added[i].first);
        unalignedStore(ptr + 4, way_idxs_added[i].second);
        ptr += 8;
      }
    }
  }

  bool operator<(const Relation_Delta& a) const noexcept
  {
    return this->id < a.id;
  }

  bool operator==(const Relation_Delta& a) const noexcept
  {
    return this->id == a.id;
  }

  template <class T, class Object>
  using Handle_Methods = Relation_Delta_Handle_Methods<T, Object>;
};


template <class T, class Object>
struct Relation_Delta_Handle_Methods
{
  typename Object::Id_Type inline id() const {
     return (static_cast<const T*>(this)->apply_func(Relation_Delta_Id_Functor<typename Object::Id_Type>()));
  }

private:
  template <typename Id_Type >
  struct Relation_Delta_Id_Functor {
    Relation_Delta_Id_Functor() = default;

    using reference_type = Relation_Delta;

    Id_Type operator()(const void* data) const
     {
       return unalignedLoad<Id_Type>(data);
     }
  };
};


#endif
