/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__STATEMENTS__RUNTIME_VALUE_H
#define DE__OSM3S___OVERPASS_API__STATEMENTS__RUNTIME_VALUE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#undef VERSION
#endif

#include "evaluator.h"
#include "statement.h"

#include <map>
#include <string>


/* === Set Key-Value Evaluator ===

Sets can have not only members.
The may get assigned properties by other statements.

An example is the property "val":
This property contains inside a "for"-loop the respective value of the current loop.

The syntax is

  <Set>.<Property>

As opposed to most other situations, it is mandatory to explicitly state the set.

*/

class Evaluator_Set_Key : public Evaluator
{
public:
  struct Statement_Maker : public Generic_Statement_Maker< Evaluator_Set_Key >
  {
    Statement_Maker() : Generic_Statement_Maker< Evaluator_Set_Key >("eval-set-key") {}
  };
  static Statement_Maker statement_maker;

  struct Evaluator_Maker : public Statement::Evaluator_Maker
  {
    Statement* create_evaluator(const Token_Node_Ptr& tree_it, QL_Context tree_context,
        Statement::Factory& stmt_factory, Parsed_Query& global_settings, Error_Output* error_output) override;
    Evaluator_Maker() { Statement::maker_by_token()["."].push_back(this); }
  };
  static Evaluator_Maker evaluator_maker;

  Evaluator_Set_Key(int line_number_, const std::map< std::string, std::string >& input_attributes,
      Parsed_Query& global_settings);// : Evaluator(line_number_) {}

#ifdef HAVE_OVERPASS_XML
  std::string dump_xml(const std::string& indent) const override
  {
    return indent + "<eval-set-key from=\"" + input + "\" key=\"" + escape_xml(key) + "\"/>\n";
  }
#endif

  std::string dump_compact_ql(const std::string&) const override
  {
    return input + "." + key;
  }

  Statement::Eval_Return_Type return_type() const override { return Statement::string; };
  std::string get_name() const override { return "eval-set-key"; }
  std::string get_result_name() const override { return ""; }

  void execute(Resource_Manager& rman) override {}

  Requested_Context request_context() const override
  {
    return Requested_Context().add_usage(input, Set_Usage::SET_KEY_VALUES);
  }

  Eval_Task* get_string_task(Prepare_Task_Context& context, const std::string*) override;
  Eval_Geometry_Task* get_geometry_task(Prepare_Task_Context& context) override { return nullptr; }

private:
  std::string input;
  std::string key;
};


#endif
