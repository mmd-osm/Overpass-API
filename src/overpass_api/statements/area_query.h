/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__STATEMENTS__AREA_QUERY_H
#define DE__OSM3S___OVERPASS_API__STATEMENTS__AREA_QUERY_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#undef VERSION
#endif

#include "../data/collect_members.h"
#include "../data/utils.h"
#include "../data/way_geometry_store.h"
#include "statement.h"

#include <map>
#include <string>
#include <vector>


class Area_Query_Statement final : public Output_Statement
{
  public:
    Area_Query_Statement(int line_number_, const std::map< std::string, std::string >& attributes,
                         Parsed_Query& global_settings);
    std::string get_name() const override { return "area-query"; }
    void execute(Resource_Manager& rman) override;
    ~Area_Query_Statement() override;

    struct Statement_Maker : public Generic_Statement_Maker< Area_Query_Statement >
    {
      Statement_Maker() : Generic_Statement_Maker< Area_Query_Statement >("area-query") {}
    };
    static Statement_Maker statement_maker;

    struct Criterion_Maker : public Statement::Criterion_Maker
    {
      bool can_standalone(const std::string& type) override { return false; }
      Statement* create_criterion(const Token_Node_Ptr& tree_it,
          const std::string& type, const std::string& into,
          Statement::Factory& stmt_factory, Parsed_Query& global_settings, Error_Output* error_output) override;
      Criterion_Maker() { Statement::maker_by_ql_criterion()["area"] = this; }
    };
    static Criterion_Maker criterion_maker;

    Query_Constraint* get_query_constraint() override;

    void get_ranges
      (std::set< Uint31_Index >& area_blocks_req,
       Resource_Manager& rman);
    unsigned int count_ranges(Resource_Manager& rman);

    void get_ranges
      (const std::map< Uint31_Index, std::vector< Area_Skeleton > >& input_areas,
       const std::map< Uint31_Index, std::vector< Area_Block > >& input_area_blocks,
       std::set< Uint31_Index >& area_blocks_req,
       Resource_Manager& rman);

    void get_ranges
      (const std::map< Uint31_Index, std::vector< Way_Skeleton > >& input_ways,
       const std::map< Uint31_Index, std::vector< Area_Skeleton > >& input_areas,
       std::set< Uint31_Index >& area_blocks_req,
       Resource_Manager& rman);

    template< typename Node_Skeleton >
    void collect_nodes
      (std::map< Uint32_Index, std::vector< Node_Skeleton > >& nodes,
       const std::set< Uint31_Index >& req, bool add_border,
       Resource_Manager& rman);

    template< typename Node_Skeleton >
    void collect_nodes_db
      (std::map< Uint32_Index, std::vector< Node_Skeleton > >& nodes,
       const std::set< Uint31_Index >& req,
       IdSetHybrid< Node::Id_Type::Id_Type> & nodes_inside,
       bool add_border,
       Resource_Manager& rman);

    template< typename Node_Skeleton >
    void collect_nodes_adhoc
      (std::map< Uint32_Index, std::vector< Node_Skeleton > >& nodes,
       const std::set< Uint31_Index >& req,
       IdSetHybrid< Node::Id_Type::Id_Type> & nodes_inside,
       bool add_border,
       Resource_Manager& rman);

    template< typename Way_Skeleton >
    void collect_ways
      (Way_Geometry_Store way_geometries,
       std::map< Uint31_Index, std::vector< Way_Skeleton > >& ways,
       const std::set< Uint31_Index >& req, bool add_border,
       const Statement& query, Resource_Manager& rman);

    void collect_ways_db
      (const std::set< Uint31_Index >& req,
       const std::map< Uint31_Index, std::vector< Area_Block > > & way_segments,
       const std::map< uint32, std::vector< std::pair< uint32, Way::Id_Type > > > & way_coords_to_id,
       std::map< Way::Id_Type, bool > & ways_inside,
       bool add_border,
       Resource_Manager& rman);

    void collect_ways_adhoc
      (const std::map< Uint31_Index, std::vector< Area_Block > > & way_segments,
       const std::map< uint32, std::vector< std::pair< uint32, Way::Id_Type > > > & way_coords_to_id,
       std::map< Way::Id_Type, bool > & ways_inside,
       bool add_border,
       Resource_Manager& rman);

    bool areas_from_input() const { return (submitted_id == 0); }
    long long get_submitted_id() const { return submitted_id; }
    std::string get_input() const { return input; }

    static bool is_used() { return area_stmt_ref_counter_ > 0; }

#ifdef HAVE_OVERPASS_XML
    std::string dump_xml(const std::string& indent) const override
    {
      return indent + "<area-query"
          + (input != "_" ? std::string(" from=\"") + input + "\"" : "")
          + (submitted_id > 0 ? std::string(" ref=\"") + to_string(submitted_id) + "\"" : "")
          + dump_xml_result_name() + "/>\n";
    }
#endif

    std::string dump_compact_ql(const std::string&) const override
    {
      return "node" + dump_ql_in_query("") + dump_ql_result_name() + ";";
    }
    std::string dump_ql_in_query(const std::string&) const override
    {
      return std::string("(area")
          + (input != "_" ? std::string(".") + input : "")
          + (submitted_id > 0 ? std::string(":") + to_string(submitted_id) : "")
          + ")";
    }
    std::string dump_pretty_ql(const std::string& indent) const override { return indent + dump_compact_ql(indent); }

  private:
    std::string input;
    long long submitted_id;
    std::vector< Way_Skeleton::Id_Type > way_areas_id;    
    std::vector< Area_Skeleton::Id_Type > area_id;       // all area ids
    std::vector< Area_Skeleton::Id_Type > area_id_db;    // area ids for areas which have been persisted on disk
    std::vector< Area_Skeleton::Id_Type > area_id_adhoc; // ad hoc area ids (only in memory, will be discarded after each query)
    static int area_stmt_ref_counter_;
    std::set< Uint31_Index > area_blocks_req;
    bool area_blocks_req_filled;
    std::vector< Query_Constraint* > constraints;

    void fill_ranges(Resource_Manager& rman);
};


int intersects_inner(const Area_Block& string_a, const Area_Block& string_b);

void has_inner_points(const Area_Block& string_a, const Area_Block& string_b, int& inside);

#endif
