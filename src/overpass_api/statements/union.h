/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__STATEMENTS__UNION_H
#define DE__OSM3S___OVERPASS_API__STATEMENTS__UNION_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#undef VERSION
#endif

#include <map>
#include <string>
#include <vector>

#include "statement.h"


class Union_Statement final : public Output_Statement
{
  public:
    Union_Statement(int line_number_, const std::map< std::string, std::string >& input_attributes,
                    Parsed_Query& global_settings);
    void add_statement(Statement* statement, std::string text) override;
    std::string get_name() const override { return "union"; }
    void execute(Resource_Manager& rman) override;
    ~Union_Statement() override = default;

    static Generic_Statement_Maker< Union_Statement > statement_maker;

#ifdef HAVE_OVERPASS_XML
    std::string dump_xml(const std::string& indent) const override
    {
      std::string result = indent + "<union" + dump_xml_result_name() + ">\n";

      for (auto it = substatements.begin(); it != substatements.end(); ++it)
        result += *it ? (*it)->dump_xml(indent + "  ") : "";

      return result + indent + "</union>\n";
    }
#endif

    std::string dump_compact_ql(const std::string& indent) const override
    {
      std::string result = indent + "(";

      for (auto it = substatements.begin(); it != substatements.end(); ++it)
        result += (*it)->dump_compact_ql(indent);
      result += ")";

      return result + dump_ql_result_name() + ";";
    }

    std::string dump_pretty_ql(const std::string& indent) const override
    {
      std::string result = indent + "(";

      for (auto it = substatements.begin(); it != substatements.end(); ++it)
        result += "\n" + (*it)->dump_pretty_ql(indent + "  ");
      result += "\n)";

      return result + dump_ql_result_name() + ";";
    }

    bool union_fast_path(Resource_Manager& rman);

  private:
    std::vector< Statement* > substatements;
};


#endif
