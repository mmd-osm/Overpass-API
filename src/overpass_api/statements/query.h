/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__STATEMENTS__QUERY_H
#define DE__OSM3S___OVERPASS_API__STATEMENTS__QUERY_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#undef VERSION
#endif

#include <map>
#include <string>
#include <vector>
#include "../../expat/escape_json.h"
#include "../../expat/escape_xml.h"
#include "statement.h"
#include "../data/abstract_processing.h"

const int QUERY_NODE = 1;
const int QUERY_WAY = 2;
const int QUERY_CLOSED_WAY = 4;
const int QUERY_RELATION = 8;
const int QUERY_DERIVED = 16;
const int QUERY_AREA = 32;


typedef enum { nothing, /*ids_collected,*/ ranges_collected, data_collected } Answer_State;


class Regular_Expression;
class Bbox_Query_Statement;


/* === The Query Statement ===

The most important statement is the ''query'' statement. This is not a single statement but rather consists of one of the type specifiers ''node'', ''way'', ''relation'' (or shorthand ''rel''), ''derived'', ''area'', or ''nwr'' (shorthand for nodes, ways or relations) followed by one or more filters. The result set is the set of all elements that match the conditions of all the filters.

Example:

<source lang="cpp">
// one filter
  node[name="Foo"];
  way[name="Foo"];
  rel[name="Foo"];
  nwr[name="Foo"];
  derived[name="Foo"];
  area[name="Foo"];

// many filters
  node[name="Foo"][type="Bar"];
</source>

Here, ''node'', ''way'', ''rel'', ''nwr'', ''derived'', and ''area'' are the type specifier, ''[name="Foo"]'' resp. ''[type="Bar"]'' is the filter and the semicolon ends the statement.

The ''query'' statement has a result set that can be changed with the usual postfix notation.

<source lang="cpp">
  node[name="Foo"]->.a;
</source>

The individual filters may have in addition input sets that can be changed in the individual filters. Please see for this at the respective filter.
*/

class Query_Statement final : public Output_Statement
{
  public:
    Query_Statement(int line_number_, const std::map< std::string, std::string >& input_attributes,
                    Parsed_Query& global_settings);
    ~Query_Statement() override;

    void add_statement(Statement* statement, std::string text) override;
    std::string get_name() const override { return "query"; }
    void execute(Resource_Manager& rman) override;

    static Generic_Statement_Maker< Query_Statement > statement_maker;

    static bool area_query_exists() { return area_query_ref_counter_ > 0; }

    static const char* to_string(int type)
    {
      if (type == QUERY_NODE)
        return "node";
      else if (type == QUERY_WAY)
        return "way";
      else if (type == QUERY_RELATION)
        return "relation";
      else if (type == QUERY_DERIVED)
        return "derived";
      else if (type == QUERY_AREA)
        return "area";
      else if (type == (QUERY_NODE | QUERY_WAY | QUERY_RELATION))
        return "nwr";
      else if (type == (QUERY_NODE | QUERY_WAY))
        return "nw";
      else if (type == (QUERY_WAY | QUERY_RELATION))
        return "wr";
      else if (type == (QUERY_NODE | QUERY_RELATION))
        return "nr";

      return "area";
    }

#ifdef HAVE_OVERPASS_XML
    std::string dump_xml(const std::string& indent) const override
    {
      std::string result = indent + "<query" + dump_xml_result_name() + " type=\"" + to_string(type) + "\">\n";

      for (auto it = substatements.begin(); it != substatements.end(); ++it)
        result += *it ? (*it)->dump_xml(indent + "  ") : "";

      return result + indent + "</query>\n";
    }
#endif

    std::string dump_compact_ql(const std::string& indent) const override { return dump_subquery_map_ql(indent, false); }
    std::string dump_pretty_ql(const std::string& indent) const override { return dump_subquery_map_ql(indent, true); }

    std::string dump_subquery_map_ql(const std::string& indent, bool pretty) const
    {
      std::string result = (pretty ? indent :  "") + to_string(type);

      uint proper_substatement_count = 0;
      for (auto it = substatements.begin();
          it != substatements.end(); ++it)
      {
        if ((*it)->get_name() == "item")
          result += (*it)->dump_ql_in_query("");
        else
          ++proper_substatement_count;
      }

      std::string prefix = (pretty && proper_substatement_count > 1 ? "\n  " + indent : "");

      for (auto it = substatements.begin();
          it != substatements.end(); ++it)
      {
        if ((*it)->get_name() != "item")
          result += prefix + (*it)->dump_ql_in_query("");
      }

      if (indent == "(bbox)" && type != QUERY_AREA)
        result += "(bbox)";

      return result + (pretty && proper_substatement_count > 1 && !dump_ql_result_name().empty() ? "\n  " + indent : "")
          + dump_ql_result_name() + ";";
    }

  private:
    int type;
    std::vector< std::string > keys;
    std::vector< std::pair< std::string, std::string > > key_values;
    std::vector< std::pair< std::string, Regular_Expression* > > key_regexes;
    std::vector< std::pair< Regular_Expression*, Regular_Expression* > > regkey_regexes;
    std::vector< std::pair< std::string, std::string > > key_nvalues;
    std::vector< std::pair< std::string, Regular_Expression* > > key_nregexes;
    std::vector< std::pair< Regular_Expression*, Regular_Expression* > > regkey_nregexes;
    std::vector< Query_Constraint* > constraints;
    std::vector< Statement* > substatements;
    Bbox_Query_Statement* global_bbox_statement = nullptr;

    static int area_query_ref_counter_;

    template< typename Skeleton, typename Id_Type >
    std::vector< std::pair< Id_Type, Uint31_Index > > collect_ids
        (Resource_Manager& rman, timestamp_t timestamp, Query_Filter_Strategy& check_keys_late, bool& result_valid);

    template< typename Skeleton, class Id_Type >
    std::vector< Id_Type > collect_ids
        (Resource_Manager& rman, Query_Filter_Strategy check_keys_late);

    template< typename Skeleton, class Id_Type >
    IdSetHybrid<typename Id_Type::Id_Type> collect_non_ids_hybrid
       (Resource_Manager& rman, timestamp_t timestamp,
        Query_Filter_Strategy& check_keys_late, bool& result_valid);

    template< typename Skeleton, class Id_Type >
    std::vector< Id_Type > collect_non_ids(Resource_Manager& rman, timestamp_t timestamp);

    template< typename Skeleton, class Id_Type >
    std::vector< Id_Type > collect_non_ids(Resource_Manager& rman);

    void get_elements_by_id_from_db
        (std::map< Uint31_Index, std::vector< Area_Skeleton > >& elements,
	 const std::vector< Area_Skeleton::Id_Type >& ids, bool invert_ids,
         Resource_Manager& rman, File_Properties& file_prop);

    template< class TIndex, class TObject >
    void filter_by_tags
        (std::map< TIndex, std::vector< TObject > >& items,
         std::map< TIndex, std::vector< Attic< TObject > > >* attic_items,
         timestamp_t timestamp,
         const File_Properties& file_prop, const File_Properties* attic_file_prop,
         Resource_Manager& rman, Transaction& transaction);

    template< class TIndex, class TObject >
    void filter_by_tags
        (std::map< TIndex, std::vector< TObject > >& items,
         const File_Properties& file_prop,
         Resource_Manager& rman, Transaction& transaction);

    void filter_by_tags(std::map< Uint31_Index, std::vector< Derived_Structure > >& items);

    template< typename Skeleton, typename Id_Type, typename Index >
    void progress_1(std::vector< Id_Type >& ids, std::vector< Index >& range_req,
                    bool& invert_ids, timestamp_t timestamp,
                    Answer_State& answer_state, Query_Filter_Strategy& check_keys_late,
                    Resource_Manager& rman);

    template< typename Skeleton, class Id_Type >
    void progress_1(std::vector< Id_Type >& ids, bool& invert_ids,
                    Answer_State& answer_state, Query_Filter_Strategy check_keys_late,
                    Resource_Manager& rman);

    template< class Id_Type >
    void collect_nodes(std::vector< Id_Type >& ids,
				 bool& invert_ids, Answer_State& answer_state, Set& into,
				 Resource_Manager& rman);

    template< class Id_Type >
    void collect_elems(int type, std::vector< Id_Type >& ids,
				 bool& invert_ids, Answer_State& answer_state, Set& into,
				 Resource_Manager& rman);

    void collect_elems(Answer_State& answer_state, Set& into, Resource_Manager& rman);
    void apply_all_filters(
        Resource_Manager& rman, timestamp_t timestamp, Query_Filter_Strategy check_keys_late, Set& into);
};


class Has_Kv_Statement : public Statement
{
  public:
    Has_Kv_Statement(int line_number_, const std::map< std::string, std::string >& input_attributes,
                     Parsed_Query& global_settings);
    std::string get_name() const override { return "has-kv"; }
    std::string get_result_name() const override { return ""; }
    void execute(Resource_Manager& rman) override {}
    ~Has_Kv_Statement() override;

    static Generic_Statement_Maker< Has_Kv_Statement > statement_maker;

    std::string get_key() const { return key_regex ? "" : key; }
    Regular_Expression* get_key_regex() { return key_regex; }
    std::string get_value() const { return regex ? "" : value; }
    Regular_Expression* get_regex() { return regex; }
    bool get_straight() const { return straight; }

#ifdef HAVE_OVERPASS_XML
    std::string dump_xml(const std::string& indent) const override
    {
      return indent + "<has-kv"
          + (!key.empty() ? (key_regex ? std::string(" regk=\"") : std::string(" k=\"")) + escape_xml(key) + "\"" : "")
          + (!value.empty() ? (regex ? std::string(" regv=\"") : std::string(" v=\"")) + escape_xml(value) + "\"" : "")
          + (straight ? "" : " modv=\"not\"")
          + (case_sensitive ? "" : " case=\"ignore\"")
          + "/>\n";
    }
#endif

    std::string dump_compact_ql(const std::string&) const override
    {
      return std::string("[")
          + (key_regex ? "~\"" : "\"") + escape_cstr(key) + "\""
          + (!value.empty() ? std::string(straight ? "" : "!") + (regex ? "~\"" : "=\"") + escape_cstr(value) + "\"" : "")
          + (case_sensitive ? "" : ",i")
          + "]";
    }
    std::string dump_pretty_ql(const std::string& indent) const override { return dump_compact_ql(indent); }

  private:
    std::string key, value;
    Regular_Expression* regex = nullptr;
    Regular_Expression* key_regex = nullptr;
    bool straight;
    bool case_sensitive;
};

#endif
