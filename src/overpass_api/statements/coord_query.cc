/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cctype>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <math.h>
#include <stdlib.h>
#include <vector>

#include <iomanip>

#include "../../template_db/block_backend.h"
#include "../dispatch/resource_manager.h"
#include "../data/collect_items.h"
#include "../data/tilewise_geometry.h"
#include "coord_query.h"


int Coord_Query_Statement::coord_stmt_ref_counter_ = 0;

Generic_Statement_Maker< Coord_Query_Statement > Coord_Query_Statement::statement_maker("coord-query");

Coord_Query_Statement::Coord_Query_Statement
    (int line_number_, const std::map< std::string, std::string >& input_attributes, Parsed_Query& global_settings)
    : Output_Statement(line_number_)
{
  ++coord_stmt_ref_counter_;

  std::map< std::string, std::string > attributes;

  attributes["from"] = "_";
  attributes["into"] = "_";
  attributes["lat"] = "";
  attributes["lon"] = "";

  eval_attributes_array(get_name(), attributes, input_attributes);

  input = attributes["from"];
  set_output(attributes["into"]);

  lat = 100.0;
  lon = 200.0;
  if (!attributes["lat"].empty() || !attributes["lon"].empty())
  {
    try {
      lat = std::stod(attributes["lat"]);
      if ((lat < -90.0) || (lat > 90.0) || (attributes["lat"].empty()))
      {
        add_static_error("For the attribute \"lat\" of the element \"coord-query\" the only allowed values are floats between -90.0 and 90.0.");
      }

      lon = std::stod(attributes["lon"]);
      if ((lon < -180.0) || (lon > 180.0) || (attributes["lon"].empty()))
      {
        add_static_error("For the attribute \"lon\" of the element \"coord-query\" the only allowed values are floats between -180.0 and 180.0.");
      }
    }
    catch (std::invalid_argument&)
    {
      add_static_error("Invalid longitude/latitude value");
    }
    catch (std::out_of_range&)
    {
      add_static_error("Longitude/latitude value out of range");
    }
  }

}


/*overall strategy: we count the number of intersections of a straight line from
  the coordinates to the southern end of the block. If it is odd, the coordinate
  is inside the area, if not, they are not.
*/
int Coord_Query_Statement::check_area_block
    (uint32 ll_index, const Area_Block& area_block,
     uint32 coord_lat, int32 coord_lon)
{
  // An area block is a chain of segments. We consider each
  // segment individually. This falls into different cases, determined by
  // the relative position of the intersection of the segment and a straight
  // line from north to south through the coordinate to test.
  // (1) If the segment intersects north of the coordinates or doesn't intersect
  // at all, we can ignore it.
  // (2) If the segment intersects at exactly the coordinates, the coordinates
  // are on the boundary, hence in our definition part of the area.
  // (3) If the segment intersects south of us, it toggles whether we are inside
  // or outside the area
  // (4) A special case is if one endpoint is the intersection point. We then toggle
  // only either the western or the eastern side. We are part of the area if in the
  // end the western or eastern side have an odd state.
  int state = 0;

  const auto & ilat_ilon_pairs = area_block.get_ilat_ilon_pairs();

  auto it(ilat_ilon_pairs.begin());

  uint32 ll_index_ilat = ::ilat(ll_index, 0);
  int32 ll_index_ilon = ::ilon(ll_index, 0);

  uint32 lat = ll_index_ilat | it->first;
  int32 lon = ll_index_ilon | (it->second ^ 0x80000000);

  while (++it != ilat_ilon_pairs.end())
  {
    uint32 last_lat = lat;
    int32 last_lon = lon;

    lat = ll_index_ilat | it->first;
    lon = ll_index_ilon | (it->second ^ 0x80000000);

    if (last_lon < lon)
    {
      if (lon < coord_lon)
	continue; // case (1)
      else if (last_lon > coord_lon)
	continue; // case (1)
      else if (lon == coord_lon)
      {
	if (lat < coord_lat)
	  state ^= TOGGLE_WEST; // case (4)
	else if (lat == coord_lat)
	  return HIT; // case (2)
	// else: case (1)
	continue;
      }
      else if (last_lon == coord_lon)
      {
	if (last_lat < coord_lat)
	  state ^= TOGGLE_EAST; // case (4)
	else if (last_lat == coord_lat)
	  return HIT; // case (2)
	// else: case (1)
	continue;
      }
    }
    else if (last_lon > lon)
    {
      if (lon > coord_lon)
	continue; // case (1)
      else if (last_lon < coord_lon)
	continue; // case (1)
      else if (lon == coord_lon)
      {
	if (lat < coord_lat)
	  state ^= TOGGLE_EAST; // case (4)
	else if (lat == coord_lat)
	  return HIT; // case (2)
	// else: case (1)
	continue;
      }
      else if (last_lon == coord_lon)
      {
	if (last_lat < coord_lat)
	  state ^= TOGGLE_WEST; // case (4)
	else if (last_lat == coord_lat)
	  return HIT; // case (2)
	// else: case (1)
	continue;
      }
    }
    else // last_lon == lon
    {
      if (lon == coord_lon &&
          ((last_lat <= coord_lat && coord_lat <= lat) || (lat <= coord_lat && coord_lat <= last_lat)))
        return HIT; // case (2)
      continue; // else: case (1)
    }

    uint32 intersect_lat = lat +
        ((int64)coord_lon - lon)*((int64)last_lat - lat)/((int64)last_lon - lon);
    if (coord_lat > intersect_lat)
      state ^= (TOGGLE_EAST | TOGGLE_WEST); // case (3)
    else if (coord_lat == intersect_lat)
      return HIT; // case (2)
    // else: case (1)
  }
  return state;
}


void register_coord(double lat, double lon,
    std::set< Uint31_Index >& req, std::map< Uint31_Index, std::vector< std::pair< double, double > > >& coord_per_req)
{
  Uint31_Index idx = Uint31_Index(::ll_upper_(lat, lon) & 0xffffff00);
  req.insert(idx);
  coord_per_req[idx].push_back(std::make_pair(lat, lon));
}


struct Closedness_Predicate
{
  bool match(const Way_Skeleton& obj) const { return !obj.c_nds().empty() && obj.c_nds().front() == obj.c_nds().back(); }
  bool match(const Handle< Way_Skeleton >& h) const
  {
    auto w = h.object();
    return !w.c_nds().empty() && w.c_nds().front() == w.c_nds().back();
  }
  bool match(const Handle< Attic< Way_Skeleton > >& h) const
  {
    auto w = h.object();
    return !w.c_nds().empty() && w.c_nds().front() == w.c_nds().back();
  }
};


void Coord_Query_Statement::execute(Resource_Manager& rman)
{
  if (rman.area_updater())
    rman.area_updater()->flush();

  std::set< Uint31_Index > req;
  std::set< Uint31_Index > node_idxs;
  std::map< Uint31_Index, std::vector< std::pair< double, double > > > coord_per_req;

  const Set* input_set = nullptr;
  if (lat != 100.0)
  {
    node_idxs.insert(::ll_upper_(lat, lon));
    register_coord(lat, lon, req, coord_per_req);
  }
  else
  {
    input_set = rman.get_set(input);
    if (input_set)
    {
      const std::map< Uint32_Index, std::vector< Node_Skeleton > >& nodes = input_set->nodes;
      for (auto it = nodes.begin();
	  it != nodes.end(); ++it)
      {
        node_idxs.insert(Uint31_Index(it->first.val()));
        for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
          register_coord(::lat(it->first.val(), it2->ll_lower), ::lon(it->first.val(), it2->ll_lower),
              req, coord_per_req);
      }

      const std::map< Uint32_Index, std::vector< Attic< Node_Skeleton > > >& attic_nodes = input_set->attic_nodes;
      for (auto it = attic_nodes.begin();
          it != attic_nodes.end(); ++it)
      {
        node_idxs.insert(Uint31_Index(it->first.val()));
        for (auto it2 = it->second.begin();
            it2 != it->second.end(); ++it2)
          register_coord(::lat(it->first.val(), it2->ll_lower), ::lon(it->first.val(), it2->ll_lower),
              req, coord_per_req);
      }
    }
  }

  std::map< std::pair< double, double >, std::map< Area::Id_Type, int > > areas_inside;
  std::set< Area::Id_Type > areas_found;

  std::map< Uint31_Index, std::vector< std::pair< double, double > > >::const_iterator coord_block_it = coord_per_req.begin();
  Uint31_Index last_idx = req.empty() ? Uint31_Index(0u) : *req.begin();

  Block_Backend< Uint31_Index, Area_Block > area_blocks_db
      (rman.get_area_transaction()->data_index(area_settings().AREA_BLOCKS));

  for (const auto & it : area_blocks_db.as_discrete(req))
  {
    if (!(it.index() == last_idx))
    {
      last_idx = it.index();

      for (std::map< std::pair< double, double >, std::map< Area::Id_Type, int > >::const_iterator
	  inside_it = areas_inside.begin(); inside_it != areas_inside.end(); ++inside_it)
      {
	for (auto inside_it2 = inside_it->second.begin();
	     inside_it2 != inside_it->second.end(); ++inside_it2)
	{
	  if (inside_it2->second != 0)
	    areas_found.insert(inside_it2->first);
	}
      }
      areas_inside.clear();

      while (coord_block_it != coord_per_req.end() && coord_block_it->first < it.index())
        ++coord_block_it;
      if (coord_block_it == coord_per_req.end())
        break;
    }

    for (auto coord_it = coord_block_it->second.begin();
	 coord_it != coord_block_it->second.end(); ++coord_it)
    {
      uint32 ilat((coord_it->first + 91.0)*10000000+0.5);
      int32 ilon(coord_it->second*10000000 + (coord_it->second > 0 ? 0.5 : -0.5));

      auto obj = it.object();

      int check = check_area_block(it.index().val(), obj, ilat, ilon);
      if (check == HIT)
        areas_found.insert(it.handle().id());
      else if (check != 0)
      {
        auto it2 = areas_inside[*coord_it].find(it.handle().id());
        if (it2 != areas_inside[*coord_it].end())
	  it2->second ^= check;
        else
	  areas_inside[*coord_it].insert(std::make_pair(it.handle().id(), check));
      }
    }
  }

  for (std::map< std::pair< double, double >, std::map< Area::Id_Type, int > >::const_iterator
      inside_it = areas_inside.begin(); inside_it != areas_inside.end(); ++inside_it)
  {
    for (auto inside_it2 = inside_it->second.begin();
        inside_it2 != inside_it->second.end(); ++inside_it2)
    {
      if (inside_it2->second != 0)
        areas_found.insert(inside_it2->first);
    }
  }
  areas_inside.clear();

  Set into;

  if (!areas_found.empty())
  {
    std::vector< uint32 > req_v;
    for (auto it = req.begin(); it != req.end(); ++it)
      req_v.push_back(it->val());
    std::vector< uint32 > idx_req_v = ::calc_parents(req_v);
    std::vector< Uint31_Index > idx_req;
    for (std::vector< uint32 >::const_iterator it = idx_req_v.begin(); it != idx_req_v.end(); ++it)
      idx_req.push_back(*it);
    sort(idx_req.begin(), idx_req.end());
    Block_Backend< Uint31_Index, Area_Skeleton, std::vector< Uint31_Index >::const_iterator > area_locations_db
        (rman.get_area_transaction()->data_index(area_settings().AREAS));
    for (const auto & it : area_locations_db.as_discrete(idx_req))
    {
      if (areas_found.find(it.handle().id()) != areas_found.end())
        into.areas[it.index()].push_back(it.object());
    }
  }

  std::set< Uint31_Index > way_idxs = calc_parents(node_idxs);
  std::map< Uint31_Index, std::vector< Way_Skeleton > > current_candidates;
  std::map< Uint31_Index, std::vector< Attic< Way_Skeleton > > > attic_candidates;
  if (rman.get_desired_timestamp() == NOW)
    collect_items_discrete(
        this, rman, *osm_base_settings().WAYS, way_idxs, Closedness_Predicate(), current_candidates);
  else
    collect_items_discrete_by_timestamp(
        this, rman, way_idxs, Closedness_Predicate(), current_candidates, attic_candidates);

  if (lat != 100.0)
  {
    Tilewise_Area_Iterator tai(current_candidates, attic_candidates, *this, rman);
    uint32 idx = ::ll_upper_(lat, lon);
    tai.set_limits(ilat(idx, 0u), ilat(idx, 0u));
    while (!tai.is_end() && tai.get_idx().val() < idx)
      tai.next();
    if (!tai.is_end())
      tai.move_covering_ways(idx, ::ll_lower(lat, lon), into.ways, into.attic_ways);
  }
  else if (input_set)
  {
    Tilewise_Area_Iterator tai(current_candidates, attic_candidates, *this, rman);
    uint32 minlat = 0x7fff0000u;
    uint32 maxlat = 0u;
    for (auto it = input_set->nodes.begin();
        it != input_set->nodes.end(); ++it)
    {
      minlat = std::min(minlat, ilat(it->first.val(), 0u));
      maxlat = std::max(maxlat, ilat(it->first.val(), 0u));
    }
    for (auto it = input_set->attic_nodes.begin();
        it != input_set->attic_nodes.end(); ++it)
    {
      minlat = std::min(minlat, ilat(it->first.val(), 0u));
      maxlat = std::max(maxlat, ilat(it->first.val(), 0u));
    }
    tai.set_limits(minlat, maxlat);

    auto cur_it = input_set->nodes.begin();
    auto attic_it =
        input_set->attic_nodes.begin();

    while (cur_it != input_set->nodes.end() || attic_it != input_set->attic_nodes.end())
    {
      Uint32_Index idx =
          (attic_it == input_set->attic_nodes.end() ||
              (cur_it != input_set->nodes.end() && !(attic_it->first < cur_it->first))) ? cur_it->first : attic_it->first;
      while (!tai.is_end() && tai.get_idx().val() < idx.val())
        tai.next();
      if (tai.is_end())
        break;

      if (cur_it->first == tai.get_idx())
      {
        for (auto it2 = cur_it->second.begin(); it2 != cur_it->second.end();
            ++it2)
          tai.move_covering_ways(cur_it->first.val(), it2->ll_lower, into.ways, into.attic_ways);
      }
      if (attic_it->first == tai.get_idx())
      {
        for (auto it2 = attic_it->second.begin();
            it2 != attic_it->second.end(); ++it2)
          tai.move_covering_ways(attic_it->first.val(), it2->ll_lower, into.ways, into.attic_ways);
      }
      while (cur_it != input_set->nodes.end() && !(tai.get_idx().val() < cur_it->first.val()))
        ++cur_it;
      while (attic_it != input_set->attic_nodes.end() && !(tai.get_idx().val() < attic_it->first.val()))
        ++attic_it;
    }
  }

  transfer_output(rman, into);
  rman.health_check(*this);
}
