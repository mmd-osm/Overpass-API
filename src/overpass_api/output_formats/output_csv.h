/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__OUTPUT_FORMATS__OUTPUT_CSV_H
#define DE__OSM3S___OVERPASS_API__OUTPUT_FORMATS__OUTPUT_CSV_H


#include "../core/datatypes.h"
#include "../core/geometry.h"
#include "../frontend/output_handler.h"

#include <string>
#include <utility>
#include <vector>


struct Csv_Settings
{
  std::vector< std::pair< std::string, bool > > keyfields;
  bool with_headerline;
  std::string separator;
};


class Output_CSV : public Output_Handler
{
public:
  Output_CSV(Csv_Settings csv_settings_) : csv_settings(std::move(csv_settings_)) {}

  bool write_http_headers() override;
  void write_payload_header(const std::string& db_dir,
				    const std::string& timestamp, const std::string& area_timestamp) override;
  void write_footer() override;
  void display_remark(const std::string& text) override;
  void display_error(const std::string& text) override;

  void print_global_bbox(const Bbox_Double& bbox) override {}

  std::string dump_config() const override;

  void print_item(const Node_Skeleton& skel,
      const Opaque_Geometry& geometry,
      const std::vector< std::pair< std::string, std::string > >* tags,
      const OSM_Element_Metadata_Skeleton< Node::Id_Type >* meta,
      const user_id_name_t* users,
      Output_Mode mode,
      const Feature_Action& action = keep,
      const Node_Skeleton* new_skel = nullptr,
      const Opaque_Geometry* new_geometry = nullptr,
      const std::vector< std::pair< std::string, std::string > >* new_tags = nullptr,
      const OSM_Element_Metadata_Skeleton< Node::Id_Type >* new_meta = nullptr) override;

  void print_item(const Way_Skeleton& skel,
      const Opaque_Geometry& geometry,
      const std::vector< std::pair< std::string, std::string > >* tags,
      const OSM_Element_Metadata_Skeleton< Way::Id_Type >* meta,
      const user_id_name_t* users,
      Output_Mode mode,
      const Feature_Action& action = keep,
      const Way_Skeleton* new_skel = nullptr,
      const Opaque_Geometry* new_geometry = nullptr,
      const std::vector< std::pair< std::string, std::string > >* new_tags = nullptr,
      const OSM_Element_Metadata_Skeleton< Way::Id_Type >* new_meta = nullptr) override;

  void print_item(const Relation_Skeleton& skel,
      const Opaque_Geometry& geometry,
      const std::vector< std::pair< std::string, std::string > >* tags,
      const OSM_Element_Metadata_Skeleton< Relation::Id_Type >* meta,
      const std::map< uint32, std::string >* roles,
      const user_id_name_t* users,
      Output_Mode mode,
      const Feature_Action& action = keep,
      const Relation_Skeleton* new_skel = nullptr,
      const Opaque_Geometry* new_geometry = nullptr,
      const std::vector< std::pair< std::string, std::string > >* new_tags = nullptr,
      const OSM_Element_Metadata_Skeleton< Relation::Id_Type >* new_meta = nullptr) override;

  void print_item(const Derived_Skeleton& skel,
      const Opaque_Geometry& geometry,
      const std::vector< std::pair< std::string, std::string > >* tags,
      Output_Mode mode,
      const Feature_Action& action = keep) override;

private:
  template< typename Id_Type, typename OSM_Element_Metadata_Skeleton >
  void process_csv_line(int otype, const std::string& type, Id_Type id, const Opaque_Geometry& geometry,
    const OSM_Element_Metadata_Skeleton* meta,
    const std::vector< std::pair< std::string, std::string> >* tags,
    const user_id_name_t* users,
    const Csv_Settings& csv_settings,
    Output_Mode mode);

  template< typename OSM_Element_Metadata_Skeleton >
  void print_meta(const std::string& keyfield,
    const OSM_Element_Metadata_Skeleton& meta, const user_id_name_t* users);

  Csv_Settings csv_settings;
};


#endif
