/** Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Roland Olbricht et al.
 *
 * This file is part of Overpass_API.
 *
 * Overpass_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Overpass_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Overpass_API.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DE__OSM3S___OVERPASS_API__DATA__TAG_STORE_H
#define DE__OSM3S___OVERPASS_API__DATA__TAG_STORE_H


#include "filenames.h"
#include "../core/datatypes.h"
#include "../data/abstract_processing.h"
#include "../../template_db/block_backend.h"
#include "../../template_db/transaction.h"
#include "../data/custom_assessor.h"

#include <map>
#include <string>
#include <vector>






template< typename Index, typename Object >
class Tag_Store
{
public:
  Tag_Store(Resource_Manager& rman, bool use_global_cache = false);
  ~Tag_Store();

  void prefetch_all(const std::map< Index, std::vector< Object > >& elems);
  void prefetch_chunk(const std::map< Index, std::vector< Object > >& elems,
      typename Object::Id_Type lower_id_bound, typename Object::Id_Type upper_id_bound);
  void prefetch_all(const std::map< Index, std::vector< Attic< Object > > >& elems);
  void prefetch_chunk(const std::map< Index, std::vector< Attic< Object > > >& elems,
      typename Object::Id_Type lower_id_bound, typename Object::Id_Type upper_id_bound);
  void init_qt(const std::map< Index, std::vector< Object > >& elems);
  void prefetch_qt(const std::map< Index, std::vector< Object > >& elems, Index idx);

  const std::vector< std::pair< std::string, std::string > >* get(const Index& index, const Object& elem);

private:
  std::map< typename Object::Id_Type, std::vector< std::pair< std::string, std::string > > > tags_by_id;
  Resource_Manager* rman = nullptr;
  Transaction* transaction = nullptr;
  bool use_index = false;
  Index stored_index{};
  Ranges< Tag_Index_Local > ranges;
  std::map< uint32, std::vector< typename Object::Id_Type > > ids_by_coarse;
  std::map< uint32, std::vector< Attic< typename Object::Id_Type > > > attic_ids_by_coarse;
  Block_Backend< Tag_Index_Local, typename Object::Id_Type >* items_db = nullptr;
  typename Block_Backend< Tag_Index_Local, typename Object::Id_Type >::Range_Iterator* tag_it = nullptr;
  Block_Backend< Tag_Index_Local, Attic< typename Object::Id_Type > >* attic_items_db = nullptr;
  typename Block_Backend< Tag_Index_Local, Attic< typename Object::Id_Type > >::Range_Iterator* attic_tag_it = nullptr;
  const bool use_global_cache = false;

  std::map< Index, std::vector< Object > > filter_elements(const std::map< Index, std::vector< Object > >& elems);

  // for qt based:
  uint32 ids_by_coarse_qt_index = 0;
};


template< >
class Tag_Store< Uint31_Index, Derived_Structure >
{
public:
  Tag_Store(Transaction& transaction) {}
  Tag_Store(Resource_Manager& rman) {}
  Tag_Store() = default;

  void prefetch_all(const std::map< Uint31_Index, std::vector< Derived_Structure > >& elems) {}
  void prefetch_chunk(const std::map< Uint31_Index, std::vector< Derived_Structure > >& elems,
      Derived_Structure::Id_Type lower_id_bound, Derived_Structure::Id_Type upper_id_bound) {}

  void init_qt(const std::map< Uint31_Index, std::vector< Derived_Structure > >) {}
  void prefetch_qt(const std::map< Uint31_Index, std::vector< Derived_Structure > >, Uint31_Index idx) {}

  const std::vector< std::pair< std::string, std::string > >* get(
      const Uint31_Index& index, const Derived_Structure& elem) const { return &elem.tags; }
};


template< class Id_Type >
void collect_attic_tags
  (std::map< Id_Type, std::vector< std::pair< std::string, std::string > > >& tags_by_id,
   const Block_Backend< Tag_Index_Local, Id_Type >& current_items_db,
   typename Block_Backend< Tag_Index_Local, Id_Type >::Range_Iterator& current_tag_it,
   const Block_Backend< Tag_Index_Local, Attic< Id_Type > >& attic_items_db,
   typename Block_Backend< Tag_Index_Local, Attic< Id_Type > >::Range_Iterator& attic_tag_it,
   const std::vector< Attic< Id_Type > >& id_vec, uint32 coarse_index)
{
  std::map< Attic< Id_Type >, std::vector< std::pair< std::string, std::string > > > found_tags;

  // Collect all id-matched tag information from the current tags
  while ((!(current_tag_it == current_items_db.range_end())) &&
      (((current_tag_it.index_handle().get_index()) & 0x7fffff00) < coarse_index)) {
    ++current_tag_it;
  }
  while ((!(current_tag_it == current_items_db.range_end())) &&
      (((current_tag_it.index_handle().get_index()) & 0x7fffff00) == coarse_index))
  {
    auto it_id = std::lower_bound(id_vec.begin(), id_vec.end(), Attic< Id_Type >(current_tag_it.handle().id(), 0ull));
    auto it_id_end = std::upper_bound(id_vec.begin(), id_vec.end(), Attic< Id_Type >
            (current_tag_it.handle().id(), NOW));
    if (it_id != it_id_end) {
      auto idx = current_tag_it.index();
      found_tags[Attic< Id_Type >(current_tag_it.handle().id(), NOW)].push_back
          (std::make_pair(idx.key, idx.value));
    }
    ++current_tag_it;
  }

  // Collect all id-matched tag information that is younger than the respective timestamp from the attic tags
  while ((!(attic_tag_it == attic_items_db.range_end())) &&
      (((attic_tag_it.index_handle().get_index()) & 0x7fffff00) < coarse_index)) {
    ++attic_tag_it;
  }
  while ((!(attic_tag_it == attic_items_db.range_end())) &&
      (((attic_tag_it.index_handle().get_index()) & 0x7fffff00) == coarse_index))
  {
    auto it_id = std::lower_bound(id_vec.begin(), id_vec.end(), Attic< Id_Type >(attic_tag_it.handle().id(), 0ull));
    auto obj = attic_tag_it.object();
    auto it_id_end = std::upper_bound(id_vec.begin(), id_vec.end(), obj);
    if (it_id != it_id_end) {
      auto idx = attic_tag_it.index();
      found_tags[std::move(obj)].push_back
          (std::make_pair(idx.key, idx.value));
    }
    ++attic_tag_it;
  }

  // Actually take for each object and key of the multiple versions only the oldest valid version
  for (typename std::map< Attic< Id_Type >, std::vector< std::pair< std::string, std::string > > >
          ::const_iterator
      it = found_tags.begin(); it != found_tags.end(); ++it)
  {
    auto it_id = std::lower_bound(id_vec.begin(), id_vec.end(), Attic< Id_Type >(it->first, 0ull));
    auto it_id_end = std::upper_bound(id_vec.begin(), id_vec.end(), it->first);
    while (it_id != it_id_end)
    {
      std::vector< std::pair< std::string, std::string > >& obj_vec = tags_by_id[*it_id];
      auto last_added_it = it->second.end();
      for (auto it_source = it->second.begin(); it_source != it->second.end(); ++it_source)
      {
        if (last_added_it != it->second.end())
        {
          if (last_added_it->first == it_source->first)
          {
            obj_vec.push_back(*it_source);
            continue;
          }
          else
            last_added_it = it->second.end();
        }

        std::vector< std::pair< std::string, std::string > >::const_iterator it_obj = obj_vec.begin();
        for (; it_obj != obj_vec.end(); ++it_obj)
        {
          if (it_obj->first == it_source->first)
            break;
        }
        if (it_obj == obj_vec.end())
        {
          obj_vec.push_back(*it_source);
          last_added_it = it_source;
        }
      }
      ++it_id;
    }
  }

  // Remove empty tags. They are placeholders for tags added later than each timestamp in question.
  for (auto it_obj = tags_by_id.begin(); it_obj != tags_by_id.end(); ++it_obj)
  {
    for (typename std::vector< std::pair< std::string, std::string > >::size_type
        i = 0; i < it_obj->second.size(); )
    {
      if (it_obj->second[i].second == void_tag_value())
      {
        it_obj->second[i] = it_obj->second.back();
        it_obj->second.pop_back();
      }
      else
        ++i;
    }
  }
}


template< class Id_Type >
void collect_attic_tags
  (std::map< Id_Type, std::vector< std::pair< std::string, std::string > > >& tags_by_id,
   const Block_Backend< Tag_Index_Local, Id_Type >& current_items_db,
   typename Block_Backend< Tag_Index_Local, Id_Type >::Range_Iterator& current_tag_it,
   const Block_Backend< Tag_Index_Local, Attic< Id_Type > >& attic_items_db,
   typename Block_Backend< Tag_Index_Local, Attic< Id_Type > >::Range_Iterator& attic_tag_it,
   std::map< uint32, std::vector< Attic< Id_Type > > >& ids_by_coarse,
   uint32 coarse_index,
   Id_Type lower_id_bound, Id_Type upper_id_bound)
{
  std::vector< Attic< Id_Type > > id_vec = ids_by_coarse[coarse_index];

  if (!(upper_id_bound == Id_Type()))
  {
    auto it_id = std::lower_bound(id_vec.begin(), id_vec.end(),
                           Attic< Id_Type >(upper_id_bound, 0ull));
    id_vec.erase(it_id, id_vec.end());
  }
  if (!(lower_id_bound == Id_Type()))
  {
    auto it_id = std::lower_bound(id_vec.begin(), id_vec.end(),
                           Attic< Id_Type >(lower_id_bound, 0ull));
    id_vec.erase(id_vec.begin(), it_id);
  }

  collect_attic_tags< Id_Type >(tags_by_id, current_items_db, current_tag_it, attic_items_db, attic_tag_it,
      id_vec, coarse_index);
}

/*
template< class Id_Type >
void collect_tags
  (std::map< Id_Type, std::vector< std::pair< std::string, std::string > > >& tags_by_id,
   const Block_Backend< Tag_Index_Local, Id_Type >& items_db,
   typename Block_Backend< Tag_Index_Local, Id_Type >::Range_Iterator& tag_it,
   const std::vector< Id_Type >& ids, uint32 coarse_index)
{
  while ((!(tag_it == items_db.range_end())) &&
      (((tag_it.index_handle().get_index()) & 0x7fffff00) < coarse_index))
    ++tag_it;
  while ((!(tag_it == items_db.range_end())) &&
      (((tag_it.index_handle().get_index()) & 0x7fffff00) == coarse_index))
  {
    Id_Type current(tag_it.handle().id());     // avoid creating a new object instance via object()
    if ((binary_search(ids.begin(), ids.end(), current)))
    {
      auto elem = tag_it.index();
      tags_by_id[current].push_back
          (std::make_pair(std::move(elem.key), std::move(elem.value)));
    }
    ++tag_it;
  }
}
*/


template< class Id_Type >
void collect_tags_single
  (std::map< Id_Type, std::vector< std::pair< std::string, std::string > > >& tags_by_id,
   const Block_Backend< Tag_Index_Local, Id_Type >& items_db,
   typename Block_Backend< Tag_Index_Local, Id_Type >::Range_Iterator& tag_it,
   const Id_Type id, uint32 coarse_index)
{
  // Skip indices as long as index isn't matching coarse_index
  while (!(tag_it == items_db.range_end()))
  {
    if (tag_it.start_of_new_index()) {
      if (!((tag_it.index_handle().get_index() & 0x7fffff00) < coarse_index)) {
        break;
      }
      // we don't want to check any of the Id_Type for this index
      // tell templatedb we want to jump right to the next index
      tag_it.skip_current_index();
    }
    ++tag_it;
  }

  while (!(tag_it == items_db.range_end()))
  {
    if (tag_it.start_of_new_index()) {
      // index is no longer matching coarse_index -> we're done.
      if (!((tag_it.index_handle().get_index() & 0x7fffff00) == coarse_index)) {
        break;
      }
    }

    Id_Type current(tag_it.handle().id());     // avoid creating a new object instance via object()
    if (current == id)
    {
      auto elem = tag_it.index();
      tags_by_id[current].push_back
          (std::make_pair(std::move(elem.key), std::move(elem.value)));
      tag_it.skip_current_index();             // we've already found "id" in current index, let's move on to the next index
    }
    ++tag_it;
  }
}

template< class Id_Type >
void collect_tags
  (std::map< Id_Type, std::vector< std::pair< std::string, std::string > > >& tags_by_id,
   const Block_Backend< Tag_Index_Local, Id_Type >& items_db,
   typename Block_Backend< Tag_Index_Local, Id_Type >::Range_Iterator& tag_it,
   const std::vector< Id_Type >& ids, uint32 coarse_index)
{
  // Special case for single id value, avoids binary search and enables early jump to the next index
  if (ids.size() == 1) {
    collect_tags_single(tags_by_id, items_db, tag_it, ids.front(), coarse_index);
    return;
  }

  while (!(tag_it == items_db.range_end()))
  {
    if (tag_it.start_of_new_index()) {
      if (!((tag_it.index_handle().get_index() & 0x7fffff00) < coarse_index)) {
        break;
      }
      tag_it.skip_current_index();
    }
    ++tag_it;
  }

  while (!(tag_it == items_db.range_end()))
  {

    if (tag_it.start_of_new_index()) {
      if (!((tag_it.index_handle().get_index() & 0x7fffff00) == coarse_index)) {
        break;
      }
    }

    Id_Type current(tag_it.handle().id());     // avoid creating a new object instance via object()
    if (monobound_binary_search(ids, current))
    {
      auto elem = tag_it.index();
      tags_by_id[current].push_back
          (std::make_pair(std::move(elem.key), std::move(elem.value)));
    }
    ++tag_it;
  }
}


template< class Id_Type >
void collect_tags_framed
  (std::map< Id_Type, std::vector< std::pair< std::string, std::string > > >& tags_by_id,
   const Block_Backend< Tag_Index_Local, Id_Type >& items_db,
   typename Block_Backend< Tag_Index_Local, Id_Type >::Range_Iterator& tag_it,
   std::map< uint32, std::vector< Id_Type > >& ids_by_coarse,
   uint32 coarse_index,
   Id_Type lower_id_bound, Id_Type upper_id_bound)
{
  const std::vector< Id_Type > & ids_by_coarse_ref = ids_by_coarse[coarse_index];

  while (!(tag_it == items_db.range_end()))
  {
    if (tag_it.start_of_new_index()) {
      if (!((tag_it.index_handle().get_index() & 0x7fffff00) < coarse_index)) {
        break;
      }
      tag_it.skip_current_index();
    }
    ++tag_it;
  }

  while (!(tag_it == items_db.range_end()))
  {
    if (tag_it.start_of_new_index()) {
      if (!((tag_it.index_handle().get_index() & 0x7fffff00) == coarse_index)) {
        break;
      }
    }

    Id_Type current(tag_it.handle().id());     // avoid creating a new object instance via object()

    if (!(current < lower_id_bound) &&
      (current < upper_id_bound) &&
       binary_search(ids_by_coarse_ref.begin(), ids_by_coarse_ref.end(), current))
    {
      auto elem = tag_it.index();
      tags_by_id[current].push_back
          (std::make_pair(std::move(elem.key), std::move(elem.value)));
    }
    ++tag_it;
  }
}



template< typename Index, typename Object >
Tag_Store< Index, Object >::Tag_Store(Resource_Manager& rman, bool use_global_cache)
    : rman(&rman), transaction(rman.get_transaction()), use_index(false), ranges({}), use_global_cache(use_global_cache) {}

template< typename Index, typename Object >
std::map< Index, std::vector< Object > > Tag_Store< Index, Object >::filter_elements(const std::map< Index, std::vector< Object > >& elems)
{
  // Returns only elements which are not yet available in global cache

  if (!use_global_cache) {
    return elems;
  }

  auto & tags_by_id = rman->tags_by_id().get<Object>();

  std::map< Index, std::vector< Object > > elems_filtered;

  for (const auto & elem : elems) {
    auto & el_filtered = elems_filtered[elem.first.val()];

    for (const auto & el : elem.second) {
      auto it = tags_by_id.find(el.id);
      if (it == tags_by_id.end()) {
        el_filtered.push_back(el);
      }
    }
  }
  return elems_filtered;
}

template< typename Index, typename Object >
void Tag_Store< Index, Object >::prefetch_all(const std::map< Index, std::vector< Object > >& elems)
{
  auto & tags_by_id = use_global_cache ? rman->tags_by_id().get<Object>() : this->tags_by_id;

  if (elems.empty()) {
    if (!use_global_cache)
      tags_by_id.clear();
    use_index = false;
    return;
  }

  generate_ids_by_coarse(ids_by_coarse, use_global_cache ? filter_elements(elems) : elems, true);

  use_index = true;

  ranges = formulate_range_query(ids_by_coarse);

  if (ranges.empty()) {
    use_index = false;
    return;
  }

  delete items_db;
  items_db = new Block_Backend< Tag_Index_Local, typename Object::Id_Type >(
      transaction->data_index(current_local_tags_file_properties< Object >()));

  delete tag_it;
  tag_it = new typename Block_Backend< Tag_Index_Local, typename Object::Id_Type >::Range_Iterator(
      items_db->range_begin(ranges));

  if (!ids_by_coarse.empty())
  {
    if (!use_global_cache)
      tags_by_id.clear();
    stored_index = ids_by_coarse.begin()->first;
    collect_tags< typename Object::Id_Type >(tags_by_id, *items_db, *tag_it,
        ids_by_coarse[stored_index.val()], stored_index.val());
  }
}

template< typename Index, typename Object >
void Tag_Store< Index, Object >::init_qt(const std::map< Index, std::vector< Object > >& elems)
{
  auto & tags_by_id = use_global_cache ? rman->tags_by_id().get<Object>() : this->tags_by_id;

  if (elems.empty()) {
    if (!use_global_cache)
      tags_by_id.clear();
    use_index = false;
    return;
  }

  std::set< uint32 > coarse_indices;
  for (auto const& [index, vector] : elems) {
    if (!vector.empty()) {
      coarse_indices.insert(index.val() & 0x7fffff00);
    }
  }
  ranges = formulate_range_query(coarse_indices);
  // Initialize ids_by_coarse with empty vectors. They can be filled
  // on demand for a given index by calling prefetch_qt later on
  for (auto v : coarse_indices) {
    ids_by_coarse[v] = {};
  }

  use_index = true;

  if (ranges.empty()) {
    use_index = false;
    return;
  }

  delete items_db;
  items_db = new Block_Backend< Tag_Index_Local, typename Object::Id_Type >(
      transaction->data_index(current_local_tags_file_properties< Object >()));

  delete tag_it;
  tag_it = new typename Block_Backend< Tag_Index_Local, typename Object::Id_Type >::Range_Iterator(
      items_db->range_begin(ranges));

  if (!ids_by_coarse.empty())
  {
    if (!use_global_cache)
      tags_by_id.clear();
    stored_index = ids_by_coarse.begin()->first;
    prefetch_qt(elems, stored_index);
    collect_tags< typename Object::Id_Type >(tags_by_id, *items_db, *tag_it,
        ids_by_coarse[stored_index.val()], stored_index.val());
  }
}


template< typename Index, typename Object >
void Tag_Store< Index, Object >::prefetch_qt(const std::map< Index, std::vector< Object > >& elems, Index idx)
{
  auto coarse_index = idx.val() & 0x7fffff00;

  // ids_by_coarse already populated for requested index?
  if (ids_by_coarse_qt_index == coarse_index) {
    return;
  }

  if (ids_by_coarse_qt_index != 0) {
    // free memory for no longer needed previous entry
    std::vector< typename Object::Id_Type >().swap(ids_by_coarse[ids_by_coarse_qt_index]);
  }

  // Check that we've already seen this index in init_qt method previously
  if (ids_by_coarse.find(coarse_index) == ids_by_coarse.end()) {
    return;
  }

  // Prepare ids_by_coarse entry for current coarse index
  auto & ids_by_coarse_ = ids_by_coarse[coarse_index];

  {
    auto it_start = elems.lower_bound(coarse_index);
    auto it_end   = elems.upper_bound(coarse_index | 0xff);

    for (auto it = it_start; it != it_end; ++it) {
      for (auto it2(it->second.begin()); it2 != it->second.end(); ++it2) {
        ids_by_coarse_.push_back(it2->id);
      }
    }
  }

  {
    auto it_start = elems.lower_bound(coarse_index | 0x80000000);
    auto it_end   = elems.upper_bound(coarse_index | 0x800000ff);

    for (auto it = it_start; it != it_end; ++it) {
      for (auto it2(it->second.begin()); it2 != it->second.end(); ++it2) {
        ids_by_coarse_.push_back(it2->id);
      }
    }
  }

  std::sort(ids_by_coarse_.begin(), ids_by_coarse_.end());
  ids_by_coarse_.erase(std::unique(ids_by_coarse_.begin(), ids_by_coarse_.end()), ids_by_coarse_.end());

  ids_by_coarse_qt_index = coarse_index;
}



template< typename Index, typename Object >
void Tag_Store< Index, Object >::prefetch_chunk(const std::map< Index, std::vector< Object > >& elems,
    typename Object::Id_Type lower_id_bound, typename Object::Id_Type upper_id_bound)
{
  if (elems.empty()) {
    tags_by_id.clear();
    use_index = false;
    return;
  }

  tags_by_id.clear();

  {
    std::map< uint32, std::vector< typename Object::Id_Type > > empty_ids;
    ids_by_coarse.swap(empty_ids);
  }

  //generate std::set of relevant coarse indices
  generate_ids_by_coarse(ids_by_coarse, elems, lower_id_bound, upper_id_bound, true);

  Block_Backend< Tag_Index_Local, typename Object::Id_Type > items_db
      (transaction->data_index(current_local_tags_file_properties< Object >()));

  Ranges< Tag_Index_Local > ranges = formulate_range_query(ids_by_coarse);
  auto tag_it = items_db.range_begin(ranges);

  for (typename std::map< uint32, std::vector< typename Object::Id_Type > >::const_iterator
      it = ids_by_coarse.begin(); it != ids_by_coarse.end(); ++it)
    collect_tags_framed< typename Object::Id_Type >(tags_by_id, items_db, tag_it, ids_by_coarse, it->first,
		  lower_id_bound, upper_id_bound);
}


template< typename Index, typename Object >
void Tag_Store< Index, Object >::prefetch_all(const std::map< Index, std::vector< Attic< Object > > >& attic_items)
{
  // Note: global cache cannot be used in attic scenarios, query might use retro or adiff,
  // and execute query statements at different points in time.

  if (attic_items.empty()) {
    tags_by_id.clear();
    use_index = false;
    return;
  }

  use_index = true;

  generate_ids_by_coarse(attic_ids_by_coarse, attic_items, true);

  ranges = formulate_range_query(attic_ids_by_coarse);

  delete items_db;
  items_db = new Block_Backend< Tag_Index_Local, typename Object::Id_Type >(
      transaction->data_index(current_local_tags_file_properties< Object >()));
  delete attic_items_db;
  attic_items_db = new Block_Backend< Tag_Index_Local, Attic< typename Object::Id_Type > >(
      transaction->data_index(attic_local_tags_file_properties< Object >()));

  delete tag_it;
  tag_it = new typename Block_Backend< Tag_Index_Local, typename Object::Id_Type >::Range_Iterator(
      items_db->range_begin(ranges));
  delete attic_tag_it;
  attic_tag_it = new typename Block_Backend< Tag_Index_Local, Attic< typename Object::Id_Type > >::Range_Iterator(
      attic_items_db->range_begin(ranges));

  if (!attic_ids_by_coarse.empty())
  {
    tags_by_id.clear();
    stored_index = attic_ids_by_coarse.begin()->first;
    collect_attic_tags< typename Object::Id_Type >(tags_by_id, *items_db, *tag_it, *attic_items_db, *attic_tag_it,
        attic_ids_by_coarse[stored_index.val()], stored_index.val());
  }
}


template< typename Index, typename Object >
void Tag_Store< Index, Object >::prefetch_chunk(const std::map< Index, std::vector< Attic< Object > > >& attic_items,
    typename Object::Id_Type lower_id_bound, typename Object::Id_Type upper_id_bound)
{
  // Note: global cache cannot be used in attic scenarios, query might use retro or adiff,
  // and execute query statements at different points in time.

  if (attic_items.empty()) {
    tags_by_id.clear();
    use_index = false;
    return;
  }

  //generate std::set of relevant coarse indices
  generate_ids_by_coarse(attic_ids_by_coarse, attic_items, true);

  Block_Backend< Tag_Index_Local, typename Object::Id_Type > current_tags_db
      (transaction->data_index(current_local_tags_file_properties< Object >()));
  Block_Backend< Tag_Index_Local, Attic< typename Object::Id_Type > > attic_tags_db
      (transaction->data_index(attic_local_tags_file_properties< Object >()));

  Ranges< Tag_Index_Local > ranges = formulate_range_query(attic_ids_by_coarse);
  auto current_tag_it = current_tags_db.range_begin(ranges);
  auto attic_tag_it = attic_tags_db.range_begin(ranges);

  for (typename std::map< uint32, std::vector< Attic< typename Object::Id_Type > > >::const_iterator
      it = attic_ids_by_coarse.begin(); it != attic_ids_by_coarse.end(); ++it)
    collect_attic_tags(tags_by_id, current_tags_db, current_tag_it, attic_tags_db, attic_tag_it,
               attic_ids_by_coarse, it->first, lower_id_bound, upper_id_bound);
}


template< typename Index, typename Object >
Tag_Store< Index, Object >::~Tag_Store()
{
  delete items_db;
  delete tag_it;
  delete attic_items_db;
  delete attic_tag_it;
}


template< typename Index, typename Object >
const std::vector< std::pair< std::string, std::string > >*
    Tag_Store< Index, Object >::get(const Index& index, const Object& elem)
{
  auto & tags_by_id = use_global_cache ? rman->tags_by_id().get<Object>() : this->tags_by_id;

  // pre-check if element is already in global cache
  if (use_global_cache) {
    auto it = tags_by_id.find(elem.id);
    if (it != tags_by_id.end()) {
      return &it->second;
    }
  }

  if (use_index && !(stored_index == Index(index.val() & 0x7fffff00)))
  {
    if (Index(index.val() & 0x7fffff00) < stored_index)
    {
      delete tag_it;
      tag_it = new typename Block_Backend< Tag_Index_Local, typename Object::Id_Type >::Range_Iterator(
          items_db->range_begin(ranges));
      if (attic_items_db)
      {
        delete attic_tag_it;
        attic_tag_it = new typename Block_Backend< Tag_Index_Local, Attic< typename Object::Id_Type > >::Range_Iterator(
            attic_items_db->range_begin(ranges));
      }
    }

    if (!use_global_cache)
      tags_by_id.clear();
    stored_index = Index(index.val() & 0x7fffff00);

    if (attic_items_db)
      collect_attic_tags< typename Object::Id_Type >(tags_by_id, *items_db, *tag_it, *attic_items_db, *attic_tag_it,
          attic_ids_by_coarse[stored_index.val()], stored_index.val());
    else
      collect_tags< typename Object::Id_Type >(tags_by_id, *items_db, *tag_it,
          ids_by_coarse[stored_index.val()], stored_index.val());
  }

  auto it = tags_by_id.find(elem.id);
  if (it != tags_by_id.end())
    return &it->second;
  else
    return nullptr;
}


#endif
